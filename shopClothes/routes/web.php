<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Clothes;
use Symfony\Component\Console\Input\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::post('p','HomeController@po')->name('nm');
//FrontEnd
Route::get('/','HomeController@Index');
Route::get('/trang-chu','HomeController@Index');
Route::get('/detail/{id}','ClotheController@detail_clothe');
//Add Cart Ajax
Route::POST('/add-cart-ajax','CartController@add_cart_ajax');
Route::POST('/delete-cart-ajax','CartController@delete_cart_ajax');
Route::get('/show-cart-ajax','CartController@show_cart_ajax');
//
Route::POST('/select-product','CartController@select_product');
Route::POST('/update-quanity','CartController@update_quanity');
Route::POST('/change-quanity','ClotheController@change-quanity');
//Select-city-province
Route::POST('/select-delivery','DeliveryController@select_delivery');
Route::POST('/confirm-feeship','DeliveryController@confirm_feeship');
//
Route::POST('/change-color','ClotheController@change_color');

//Category

Route::get('/category/{id}','CategoryController@category_product');
Route::get('/layout_product_list','CategoryController@layout_product_list');
Route::get('/layout_product_grid','CategoryController@layout_product_grid');
Route::get('/fetch_data_sort','CategoryController@fetch_data_sort');
Route::POST('/load_data','CategoryController@load_data');
//Login
Route::get('login','LoginController@show_login')->name('login');
Route::get('/logout','LoginController@logout');
Route::post('login','LoginController@login')->name('login');
Route::get('/show','LoginController@show');
//
Route::post('/coupon','CouponController@coupon');
Route::post('/checkout','CheckoutCOntroller@checkout');
Route::post('/search-ajax','HomeController@search_ajax');
Route::post('/confirm-order','CheckoutController@confirm_order');
//Route::post('/date','CheckoutController@date');
Route::get('/user/purchase','PurchaseController@purchase');
Route::post('/cancel-order','PurchaseController@cancel_order');
Route::post('/detailcancel','PurchaseController@detailcancel');
//Comment
Route::post('/list-reply','CommentController@list_reply');
Route::post('/add-comment','CommentController@add_comment');
Route::post('/add-reply','CommentController@add_reply');
Route::post('/deleteComment','CommentController@delete_comment');
Route::post('/deleteReply','CommentController@delete_reply');
Route::get('/saveComment/{id}','CommentController@save_comment')->name('savecomment');

//RateComment
Route::get('/liked/{id}','RateCommentController@like')->name('liked');
Route::get('/disliked/{id}','RateCommentController@dislike')->name('disliked');



//BackEnd
Route::get('/admin','AdminController@Index');




