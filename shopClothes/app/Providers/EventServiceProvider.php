<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use App\Events\Logined;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use App\Listeners\AddCartSession;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        Logined::class => [
            AddCartSession::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
