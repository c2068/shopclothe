<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'tbl_city';
    protected $fillable = [
        'namecity','type'
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public function Provinces(){
        return $this->hasMany("App\Models\Province","Idcity","id");
    }
}


