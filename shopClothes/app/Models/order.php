<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $table = 'order';
    protected $fillable = [
        'customer_id','shipping_id','order_status','order_code','order_coupon','order_feeship','created_at'
    ];
    protected $primaryKey = 'order_id';
    public $timestamps = false;
    public function shipping(){
        return $this->belongsTo(Order::class,'shipping_id','shipping_id');
    }
  
}
