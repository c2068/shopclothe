<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class shipping extends Model
{
    protected $table = 'shipping';
    protected $fillable = [
        'shipping_name','shipping_address','shipping_phone','shipping_email','shipping_method'
    ];
    protected $primaryKey = 'shipping_id';
    public $timestamps = false;
    public function orders(){
        return $this->hasMany("App\Models\order","shipping_id","order_id");
    }
}
