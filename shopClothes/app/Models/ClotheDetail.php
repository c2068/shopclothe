<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClotheDetail extends Model
{
    protected $table = 'clothe_detail';
    protected $fillable = [
        'idClothe', 'color', 'image','quanity',
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;
  
}
