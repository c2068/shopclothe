<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';
    protected $fillable = [
        'idUser', 'idClothe','name', 'image','price','quanity','total'
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
