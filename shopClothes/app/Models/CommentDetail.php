<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommentDetail extends Model
{
    protected $table = 'comment_detail';
    protected $fillable = [
        'comment_id', 'customer_id', 'customer_name','customer_image','content','like','dislike', 'created_at','updated_at'
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
