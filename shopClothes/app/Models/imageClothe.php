<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class imageClothe extends Model
{
    protected $table = 'image_clothe';
    protected $fillable = [
        'idClothe', 'image'
    ];
    protected $primaryKey = 'id'; 
    public $timestamps = false;
}
