<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class order_details extends Model
{
    protected $table = 'order_details';
    protected $fillable = [
        'order_code','clothe_id','clothe_name','clothe_price','clothe_image','clothe_quanity'
    ];
    protected $primaryKey = 'order_details_id';
    public $timestamps = false;
}
