<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment';
    protected $fillable = [
        'customer_id', 'customer_name','customer_image','clothe_id','content','favorite','like','dislike', 'created_at','updated_at'
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public function ListReply(){
        return $this->hasMany("App\Models\CommentDetail","comment_id","id");
    }
    public function Rate(){
        return $this->hasMany("App\Models\RateComment","comment_id","id");
    }
}
