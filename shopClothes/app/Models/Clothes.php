<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clothes extends Model
{
    protected $table = 'clothes';
    protected $fillable = [
        'idCate', 'name', 'image','price',
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public function Image(){
        return $this->hasMany("App\Models\imageClothe","idClothe","id");
    }
    public function Color(){
        return $this->hasMany("App\Models\ClotheDetail","idClothe","id");
    }
    public function comment(){
        return $this->hasMany("App\Models\Comment","clothe_id","id");
    }
    public function cate(){
        return $this->belongsTo("App\Models\Category","idCate","id");
    }
}
