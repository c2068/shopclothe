<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'tbl_provinces';
    protected $fillable = [
        'nameprovince','type','IdCity'
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;
    
}
