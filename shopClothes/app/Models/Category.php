<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [
        'title'
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public function Clothes(){
        return $this->hasMany("App\Models\Clothes","idCate","id");
    }
}
