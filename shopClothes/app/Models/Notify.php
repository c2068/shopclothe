<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Clothes,Category;
use App\Models\ClotheDetail;

use App\Models\City;
use App\Models\Province;
use App\Models\Cart;

use Illuminate\Support\Facades\Redirect;
session_start();
class Notify extends Model
{
    protected $table = 'notify';
    protected $fillable = [
        'customer_id','content','status','created_at'
    ];
    protected $primaryKey = 'id';
    public $timestamps = false;
}
