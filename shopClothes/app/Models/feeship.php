<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class feeship extends Model
{
    protected $table = 'feeship';
    protected $fillable = [
        'fee_city', 'fee_province', 'fee_ship'
    ];
    protected $primaryKey = 'fee_id';
    public $timestamps = false;
}
