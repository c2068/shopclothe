<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Clothes,Category;
use App\Models\City,Province;
use App\Models\feeship;
use Illuminate\Support\Facades\Redirect;
session_start();
class DeliveryController extends Controller
{
   public function select_delivery(Request $request){
       $data = $request->all();
       if($data["idType"] == "city"){
           $output ="";
           $province = City::find($data["id"])->Provinces;
           $output .= "<option value=''>-- Chọn Quận/Huyện --</option>";
           foreach($province as $value){
            $output .= "<option value='{$value['id']}'>{$value['nameprovince']}</option>";
           }   
           echo $output;        
       }
       else{
           $output ="";
          $feeship = feeship::Where('fee_province',$data["id"])->get()->toArray();
          //$output .= $feeship['fee_ship'];
          echo $feeship[0]["fee_ship"];
         
       }
   }
   public function confirm_feeship(Request $request){
    $data = $request->all();
    $feeship = feeship::Where('fee_province',$data["province"])->get()->toArray();
    $fee = $feeship[0]["fee_ship"];
    //Session::put('feeship',$fee);
    /*C1--$total =0;
    if(Session::get("idUser")){
        foreach($data["product_id"] as $value){
            foreach(Session::get('cartUser') as $value1){
                if($value == $value1["idClothe"]){
                       $total += $value1["price"]*$value1["quanity"];
                }
            }
        }
    }else{
        foreach($data["product_id"] as $value){
            foreach(Session::get('cart') as $value1){
                if($value == $value1["id"]){
                       $total += $value1["price"]*$value1["quanity"];
                }
            }
        }
    }*/
    /*$m = $total+$fee;
    $array[] = $fee;
    $array[] = $m;
    return $array;*/
    //echo Session::get('feeship');
    echo $fee;
   }
}