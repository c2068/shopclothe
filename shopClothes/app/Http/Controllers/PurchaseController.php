<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Session;
use App\Models\Clothes;
use App\Models\Category;
use App\Models\City;
use App\Models\Province;
use App\Models\shipping;
use App\Models\order;
use App\Models\order_details;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
session_start();
class PurchaseController extends Controller
{
   public function purchase(Request $request){
      //$order = order::Where('customer_id',Session::get("idUser"))->Where('order_status',1)->get();
      if(!isset($request->type)){
         $type=1;
      }
      else{
      $type= $request->type;
      }
      $order = order::Where('customer_id',Session::get("idUser"))->Where('order_status',$type)->get();
      if($request->ajax()){
      $order = order::Where('customer_id',Session::get("idUser"))->Where('order_status',$type)->get();
      }
      $order_code= array();
      foreach($order as $key => $value){
         $order_code[$key]['code'] = $value->order_code;
         $order_code[$key]['coupon'] = $value->order_coupon;
         $order_code[$key]['feeship'] = $value->order_feeship;

      }
      $order = DB::table('order')->join('order_details','order.order_code','=','order_details.order_code')->Where('order.order_status',$type)->select('*')->get();
      if($request->ajax()){
      $order = DB::table('order')->join('order_details','order.order_code','=','order_details.order_code')->Where('order.order_status',$type)->select('*')->get();
      return View('pages.purchase.purchase')->with('order_code',$order_code)->with('order',$order)->with('type',$type);
      }
      return View('pages.purchase.show_purchase')->with('order_code',$order_code)->with('order',$order)->with('type',$type);
      
   }
   public function cancel_order(Request $request){
  
      $order = order::where('order_code',$request->order_code)->first();
      $date=$order->created_at;
      $now = Carbon::now('Asia/Ho_Chi_Minh');
      $distance = $now->diffInHours($date);
      if($distance <=12){
           $order->order_status =2;
           $order->updated_at = $now;
           $order->save();
           return redirect("/user/purchase");
           //return 0;
      }
      else{
         return 1;
      }
   }
   public function detailcancel(Request $request){
      $order= order::Where('order_code',$request->order_code)->first();
      $order_code= $order->order_code;
      $datebuy = $order->created_at;
      $datecancel = $order->updated_at;
      return View('pages.purchase.show_detailcancel')->with('order_code',$order_code)->with('datebuy',$datebuy)->with('datecancel',$datecancel);
     
   }
}
