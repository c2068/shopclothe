<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use App\Models\Clothes,Category;
use App\Models\ClotheDetail;
use App\Models\Coupon;

use Illuminate\Support\Facades\Redirect;
session_start();
class CouponController extends Controller
{
    public function coupon(Request $request){
        $coupon_name = $request->coupon;
        $coupon = Coupon::Where("coupon_code",'like',$coupon_name)->first();
        $arr=[];
        if($coupon){
            
            $arr[] = $coupon->coupon_name;
            $arr[] = $coupon->coupon_condition;
            $arr[] = $coupon->coupon_number;
            return $arr;
        }
        else{
            echo "Mã giảm giá không tồn tại";
        }
    }
}
