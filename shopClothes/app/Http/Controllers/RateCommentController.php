<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RateComment;
use Auth;
class RateCommentController extends Controller
{

    public function like($id){
        try {
            $query1 = ['user_id' => Auth::user()->id,'comment_id' => $id,'liked' => 1];
            $query0 = ['user_id' => Auth::user()->id,'comment_id' => $id,'liked' => 0];            
            if(RateComment::Where($query1)->first()){
                $rate = RateComment::Where($query1)->delete();
                $vl = 'destroy_like';
            }
            else{
                //$rate = RateComment::Where($query0)->first();
                if(!RateComment::Where($query0)->first()){
                $ratecomment = new RateComment();
                $ratecomment->comment_id = $id;
                $ratecomment->user_id = Auth::user()->id;
                $ratecomment->liked = 1;
                $ratecomment->save();
                $vl = 'add_like';
                }
                else{
                    $rate = RateComment::Where($query0)->update(['liked' => 1]);
                    $vl = 'switch_like';
                }
            }     
            return response()->json(['status' => 'success', 'action' => $vl ]);
        } catch (\Exception $e) {
            return $this->getMessage();
        }
    }
    public function dislike($id){
        try {
            $query1 = ['user_id' => Auth::user()->id,'comment_id' => $id,'liked' => 0];
            $query0 = ['user_id' => Auth::user()->id,'comment_id' => $id,'liked' => 1];            
            if(RateComment::Where($query1)->first()){
                $rate = RateComment::Where($query1)->delete();
                $vl = 'destroy_dislike';
            }
            else{
                //$rate = RateComment::Where($query0)->first();
                if(!RateComment::Where($query0)->first()){
                $ratecomment = new RateComment();
                $ratecomment->comment_id = $id;
                $ratecomment->user_id = Auth::user()->id;
                $ratecomment->liked = 0;
                $ratecomment->save();
                $vl = 'add_dislike';
                }
                else{
                    $rate = RateComment::Where($query0)->update(['liked' => 0]);
                    $vl = 'switch_dislike';
                }
            }     
            return response()->json(['status' => 'success', 'action' => $vl ]);
        } catch (\Exception $e) {
            return $this->getMessage();
        }
    }
}
