<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;
use App\Models\Clothes,Category;
use App\Models\ClotheDetail;
use App\Models\Comment;
use Illuminate\Support\Facades\Redirect;
session_start();
class ClotheController extends Controller
{
    public function detail_clothe($id){
        $clothe = Clothes::find($id);
        $image  = Clothes::find($id)->Image;
        $color = Clothes::find($id)->Color;
        $comments = Comment::Where('clothe_id',$id)->OrderBy('id','desc')->get();
        return View('pages.clothes.clothe_detail')->with('clothe',$clothe)->with('image',$image)->with('color',$color)->with('comments',$comments);
        /*echo "<pre>";
       print_r($color);
       echo "</pre>";*/
    }
    public function change_color(Request $request){
        $data = $request->all();
        $rs = ClotheDetail::find($data["id_color"],["image","quanity"]);
        return $rs;
    }
    /*public function change-quanity(Request $request){
        $idClothe= $request->id;
        $quanity= $request->quanity;
        $check = ClotheDetail::Where("id",$idClothe)->Where("quanity",)
    }*/
}