<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Models\Comment;
use App\Models\CommentDetail;
use Illuminate\Support\Facades\Redirect;
session_start();
class CommentController extends Controller
{
    public function list_reply(Request $request){
        $idcomment = $request->idcomment;
        $listcomment = Comment::find($idcomment)->ListReply;
        return View('pages.comment.list_reply')->with('listcomment',$listcomment);
    }
    public function add_comment(Request $request){
        $id = $request->id;
        $content = $request->content;
        $comment= new Comment();
        $comment->customer_id = Session::get("idUser");
        $comment->customer_name = Session::get("name");
        $comment->customer_image = Session::get("imageUser");
        $comment->clothe_id= $id;
        $comment->content= $content;
        $comment->like=0;
        $comment->dislike=0;
        $comment->created_at =  Carbon::now('Asia/Ho_Chi_Minh');
        $comment->save();
        $comments = Comment::Where('clothe_id',$id)->Orderby('id','desc')->get();
        return View('pages.comment.list_comment')->with('comments',$comments);
    }
    public function add_reply(Request $request){
        $id = $request->id;
        $content = $request->content;
        $comment_detail = new CommentDetail();
        $comment_detail->comment_id = $id;
        $comment_detail->customer_id =  Session::get("idUser");
        $comment_detail->customer_name = Session::get("name");
        $comment_detail->customer_image = Session::get("imageUser");
        $comment_detail->content = $content;
        $comment_detail->like = 0;
        $comment_detail->dislike = 0;
        $comment_detail->created_at =  Carbon::now('Asia/Ho_Chi_Minh');
        $comment_detail->save();
        $comment_detail = CommentDetail::Where('comment_id',$id)->get();
        return $comment_detail;

    }
    public function delete_comment(Request $request){
        $id = $request->id;
        $idClothe= $request->idclothe;
        $comment = Comment::find($id);
        $comment->delete();
        $comments = Comment::Where('clothe_id',$idClothe)->Orderby('id','desc')->get();
        return View('pages.comment.list_comment')->with('comments',$comments);
    }
    public function delete_reply(Request $request){
        $id = $request->id;
        $idcomment= $request->idcomment;
        $commentdetail = CommentDetail::find($id);
        $commentdetail->delete();
        $listcomment = Comment::find($idcomment)->ListReply;
        return View('pages.comment.list_reply')->with('listcomment',$listcomment);
    }
    public function save_comment(Request $request,$id){
        $comment = Comment::find($id);
        $comment->content = $request->text;
        $comment->save();
      
    }
}
