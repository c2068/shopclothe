<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Clothes;
use App\Models\Category;
use App\Models\City;
use App\Models\Province;
use App\Models\shipping;
use App\Models\order;
use App\Models\order_details;
use App\Models\Cart;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use App\Jobs\SendMailOrder;


class CheckoutController extends Controller
{
    public function checkout(Request $request){
        $data = $request->all();
        $city = City::find($data["city"]);
        $province = Province::find($data["province"]);
        return View('pages.checkout.show_checkout')->with('data',$data)->with('city',$city)->with('province',$province);
    //return $data["clothe_select"];
    }
    public function confirm_order(Request $request){
        $data = $request->all();
       $shipping = new shipping();
        $shipping->shipping_name= $data["name"];
        $shipping->shipping_email= $data["email"];
        $shipping->shipping_phone= $data["telephone"];
        //$address = $data["city"];
        $shipping->shipping_addres = "ghghgh";
        $shipping->shipping_method = $data["payment"];
        $shipping->save();

        $shipping_id = $shipping->shipping_id;
        $order_code = substr(md5(microtime()), rand(0,26),5);

        $order = new order();
        $order->customer_id = Session::get("idUser");
        $order->shipping_id = $shipping_id;
        $order->order_status = 1;
        $order->order_coupon = $data["coupon_checkout"] ?? 'no';
        $order->order_feeship = $data["feeship_checkout"];
        $order->order_code = $order_code;
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $order->created_at = now();
        $order->save();
     
        $product_id = explode(",",$data["clothe_checkout"]);
        foreach($product_id as $value){
            $order_detail = new order_details();
            $cartUser= Cart::Where('idUser',Session::get('idUser'))->get();
            foreach($cartUser as $value1){
                if($value == $value1["idClothe"]){
        $order_detail->order_code = $order_code;
        $order_detail->clothe_id = $value1["idClothe"];
        $order_detail->clothe_name = $value1["name"];
        $order_detail->clothe_price = $value1["price"];
        $order_detail->clothe_image = $value1["image"];

        $order_detail->clothe_quanity = $value1["quanity"];
        $order_detail->save();

                }
            }
        }
        dispatch(new SendMailOrder($order))->delay(now()->addSeconds(5));
        return View('pages.thanks-to-order');
        }
        public function date(Request $request){
            $data = $request->order_code;
            $date1= order::Where('order_code',$data)->first();
$date=$date1->created_at;
            Carbon::setLocale('vi'); // hiển thị ngôn ngữ tiếng việt.
            //$dt = Carbon::create(2021,8, 5, 21, 16, 10);
            //$dt2 = Carbon::create(2018, 10, 18, 13, 40, 16);
         
           $now = Carbon::now('Asia/Ho_Chi_Minh');
           echo $now;
            echo $date;
            //echo $now->diffForHumans($date); //12 phút trước    
echo $now->diffInHours($date);
        }
}
