<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Clothes,Category;
use App\Models\User;
use App\Models\Cart;
use App\Events\Logined;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Redirect;
session_start();
class LoginController extends Controller
{
    public function show_login(){
return View('pages.login.show_login');
    }
    public function login(Request $request){
        $username = $request['email'];
        $password = $request['password'];
        $validate = $request->validate([
            'email' => ['required'],
            'password' => ['required']
        ],[
            'email.required' => 'Không được để trống username',
            'password.required' => 'Không được để trống password',
        ]);
        if(Auth::attempt($validate)){
           $user = Auth::user();
           Session::put("idUser",$user->id);
           Session::put("name",$user->name);
           Session::put("imageUser",'avartar.jpg');
           event(new Logined());
           /*$cart= Cart::Where('idUser',Auth::user()->id)->get();
           Session::put('cartUser',$cart);*/
           return redirect()->intended('/');   
        }
        else{
            Session::flash('error','Tài khoản hoặc mật khẩu không đúng');         
            return back()->withErrors(['failed' => 'Tài khoản hoặc mật khẩu không đúng!'])->withInput();         
        }
    }
    public function logout(){
        Auth::logout();
        session()->flush();
        /*Session::put("idUser",null);
        Session::put("name",null);*/
        return redirect()->intended();
    }
}