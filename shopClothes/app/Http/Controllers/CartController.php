<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Clothes,Category;
use App\Models\ClotheDetail;

use App\Models\City;
use App\Models\Province;
use App\Models\Cart;

use Illuminate\Support\Facades\Redirect;
session_start();
class CartController extends Controller
{
  
    public function add_cart_ajax(Request $request){
        $data = $request->all();
        if(!isset($data["quanity"])){
            $clothes = ClotheDetail::Where("idClothe",$data["id"])->first();
            $data["id"] = $clothes->id;
        }
        //Sau khi login
        if(Session::get('idUser')){
            $check = Cart::Where("idUser",Session::get("idUser"))->Where("idClothe",$data["id"])->first();
            if($check){
                if(isset($data["quanity"])){
                   $check->quanity = $check->quanity +$data["quanity"];
                }
                else{
                   $check->quanity = $check->quanity +1;                  
                }
                $check->total = $check->quanity* $check->price;
                $check->save();
            }
            else{
                $cart = new Cart();
                $cart->idUser= Session::get('idUser');
                $cart->idClothe= $data["id"];
                $cart->name= $data["name"];
                $cart->image= $data["image"];
                $cart->price= $data["price"];
                if(isset($data["quanity"])){
                $cart->quanity= $data["quanity"];
                }
                else{
                $cart->quanity= 1; }  
                $cart->total = $data["price"]*$cart->quanity;            
                $cart->save();
            }
            /*$cart= Cart::Where('idUser',Session::get('idUser'))->get();
            if(Session::has('cartUser')){
                Session::put("cartUser",null);
            }
            Session::put('cartUser',$cart);*/

        }
        //Chưa login
        else{
            $cart = Session::get('cart');
            if($cart){
            $exits =0;
            foreach($cart as $key => $value){
             if($value["id"] == $data["id"]){
                   $exits =1;
                   $index = $key;
             }
            }
            if($exits ==1){
                if(isset($data["quanity"])){
                    $cart[$index]["quanity"] += $data["quanity"];
                }
                else{
                    $cart[$index]["quanity"] += 1;
                }
                Session::put('cart',$cart);

            }
            else{
                if(isset($data["quanity"])){
                    $cart[] = array(
                        "id" =>$data["id"],
                        "name" => $data["name"],
                        "image" =>$data["image"],
                        "price" => $data["price"],
                        "quanity" =>$data["quanity"]
                    );
                }
                else{
                    $cart[] = array(
                        "id" =>$data["id"],
                        "name" => $data["name"],
                        "image" =>$data["image"],
                        "price" => $data["price"],
                        "quanity" =>1
                    );
                }
               
                Session::put('cart',$cart);
            }
        }
        else{
            if(isset($data["quanity"])){
                $cart[] = array(
                    "id" =>$data["id"],
                    "name" => $data["name"],
                    "image" =>$data["image"],
                    "price" => $data["price"],
                    "quanity" =>$data["quanity"]
                );
            }
            else{
                $cart[] = array(
                    "id" =>$data["id"],
                    "name" => $data["name"],
                    "image" =>$data["image"],
                    "price" => $data["price"],
                    "quanity" =>1
                );
            }        
        }
        Session::put('cart',$cart);
        }
        
    }
    public function delete_cart_ajax(Request $request){
        $id = $request->id;
        //Sau khi login
        if(Session::get('idUser')){
            $cart = Cart::Where("idUser",Session::get("idUser"))->Where("idClothe",$id)->first();
            $cart->delete();
            $cart= Cart::all();
            /*if(Session::has('cartUser')){
                Session::put("cartUser",null);
            }
            Session::put('cartUser',$cart);*/
        }
        //Chưa login
        else{
            $cart = Session::get('cart');
            if($cart){
            foreach($cart as $key => $value){
                if($value["id"] == $id){
                    unset($cart[$key]);
                }
            }
            Session::put('cart',$cart);
        }
        }
        
    }
    public function show_cart_ajax(){
        $city = City::all();
        return View('pages.cart.cart')->with('city',$city);
       //Session::flush();
     
    }
    public function select_product(Request $request){
        $data = $request->product_id;
        $subtotal =0;
        if(Session::get("idUser")){
            $cartUser= Cart::Where('idUser',Session::get("idUser"))->get();
            foreach($cartUser as $value){
                foreach($data as $value1){
                    if($value1 == $value["idClothe"]){
                        $subtotal += $value["price"]* $value["quanity"];
                    }
                }
            }
        }else{
            foreach(Session::get('cart') as $key => $value){
                   foreach($data as $value1){
                       if($value1 == $value["id"]){
                           $subtotal += $value["price"]* $value["quanity"];
                       }
                   }
            }
        }
        /*if(Session::has('feeship')){
            $grandtotal = $subtotal + Session::get('feeship');
            $array[] = $subtotal;
            $array[] = $grandtotal;
            return $array;
        }
        else{
            echo $subtotal;
        }*/
        echo $subtotal;
       
    }
    public function update_quanity(Request $request){
        $data = $request->all();  
        //sau khi login
        if(Session::get("idUser")){
            $cartUser= Cart::Where('idUser',Session::get('idUser'))->get();
            Session::put("cartUser",$cartUser);
            $subtotal=0;
            if(empty($data["product_id"])){
                $total =0;
                $check = Cart::Where("idUser",Session::get("idUser"))->Where("idClothe",$data["id"])->first();
                $check->quanity= $data["quanity"];
                $check->total = $check->quanity * $check->price;
                $check->save();
                $total = $check->total;
                /*if(Session::has('cartUser')){
                    Session::put("cartUser",null);
                }
                $cart = Cart::Where('idUser',Session::get("idUser"))->get();
                Session::put('cartUser',$cart);*/
                $cartUser= Cart::Where('idUser',Session::get('idUser'))->get();
                echo $total; 
            }
            else{
                foreach($data["product_id"] as $value){
                    if($value == $data["id"]){
                       $total =0;
                       foreach($cartUser as $key => $value1){                        
                             if($value1["idClothe"] == $data["id"]){
                                $check = Cart::Where("idUser",Session::get("idUser"))->Where("idClothe",$data["id"])->first();
                                $check->quanity= $data["quanity"];
                                $check->total = $check->quanity * $check->price;
                                $check->save();
                                $total +=$check->total;
                             }
                       }
                       /*if(Session::has('cartUser')){
                        Session::put("cartUser",null);
                        }
                        $cart = Cart::Where('idUser',Session::get("idUser"))->get();
                        Session::put('cartUser',$cart);*/
                        $cartUser= Cart::Where('idUser',Session::get('idUser'))->get();

                    }else{
                       $total=0;
                       foreach($cartUser as $key => $value){
                           if($value["idClothe"] == $data["id"]){
                            $check = Cart::Where("idUser",Session::get("idUser"))->Where("idClothe",$data["id"])->first();
                            $check->quanity= $data["quanity"];
                            $check->total = $check->quanity * $check->price;
                            $check->save();
                            $total +=$check->total;
                           }
                       }       
                       /*if(Session::has('cartUser')){
                        Session::put("cartUser",null);
                        }
                        $cart = Cart::Where('idUser',Session::get("idUser"))->get();
                        Session::put('cartUser',$cart);*/   
                        $cartUser= Cart::Where('idUser',Session::get('idUser'))->get();
                    }                   
               }
               $subtotal=0;
               foreach($data["product_id"] as $value1){
                   foreach($cartUser as $value){
                       if($value1 == $value["idClothe"]){
                           $subtotal += $value["price"]*$value["quanity"];                       
                       }
                }   
               }                                                            
               $array[]=$total;
               $array[] = $subtotal;
               /*C1 if(Session::has('feeship')){
                    $grandtotal = $subtotal + Session::get('feeship');
                    $array[]= $grandtotal;
               }   */ 
               return $array;

               //
            }
        }
        //Chưa login
        else{
            $cart = Session::get('cart');
            $subtotal=0;
            if(empty($data["product_id"])){
                $total =0;
                foreach($cart as $key => $value){
                    if($value["id"] == $data["id"]){
                        $cart[$key]["quanity"] = $data["quanity"];
                        $total =$cart[$key]["quanity"]*$cart[$key]["price"];
                    }
                }       
               Session::put('cart',$cart);
               echo $total; 
            }
            else{                           
                    foreach($data["product_id"] as $value){
                         if($value == $data["id"]){
                            $total =0;
                            foreach($cart as $key => $value1){                        
                                  if($value1["id"] == $data["id"]){
                                      $cart[$key]["quanity"] = $data["quanity"];
                                      $total +=$cart[$key]["quanity"]*$cart[$key]["price"];
                                  }
                            }
                            Session::put('cart',$cart);
                         }else{
                            $total=0;
                            foreach($cart as $key => $value){
                                if($value["id"] == $data["id"]){
                                    $cart[$key]["quanity"] = $data["quanity"];
                                    $total =$cart[$key]["quanity"]*$cart[$key]["price"];
                                }
                            }       
                           Session::put('cart',$cart);                                         
                         }                   
                    }
                    $subtotal=0;
                    foreach($data["product_id"] as $value1){
                        foreach(Session::get('cart') as $value){
                            if($value1 == $value["id"]){
                                $subtotal += $value["price"]*$value["quanity"];                       
                            }
                     }   
                    }                                                            
                    $array[]=$total;
                    $array[] = $subtotal;
                    if(Session::has('feeship')){
                         $grandtotal = $subtotal + Session::get('feeship');
                         $array[]= $grandtotal;
                    }    
                    return $array;
                    //print_r(Session::get('cart'));
            }
        }
       
             
    }
    public function checkExit($idClothe){
        
    }
    public function coupon(Request $request){
        return redirect()->back();
    }
}