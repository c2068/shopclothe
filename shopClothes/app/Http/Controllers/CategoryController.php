<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Models\Clothes;
use App\Models\Category;
use Illuminate\Support\Facades\Redirect;
session_start();
class CategoryController extends Controller
{
    public function category_product($id,Request $request){
        $product_category= Category::find($id)->Clothes()->OrderBy('id','desc')->Paginate(3);
        $category = Category::all();
        if($request->ajax()){
          $product_category= Category::find($id)->Clothes()->OrderBy('id','desc')->Paginate(3);
          return View('pages.category.category_product_grid')->with('product_category',$product_category)->render();
          //print_r($product_category);     
        }
        else{
          return View('pages.category.category_product')->with('product_category',$product_category)->with('category',$category);      
        }
      
      }
      public function fetch_data_sort(Request $request){
        if($request->ajax()){
          $category = $request->category;
          $sorttype= $request->sorttype;
          $product_category= Category::find($category)->Clothes()->OrderBy('price',$sorttype)->Paginate(3);
          return View('pages.category.category_product_grid')->with('product_category',$product_category)->render();
  
        }
       
      }
    public function layout_product_list(Request $request){
        $idCate = $request->category;
        $product_category= Category::find($idCate)->Clothes()->OrderBy('id','desc')->limit(4)->get();
        return View('pages.category.category_product_list')->with('product_category',$product_category)->render();
    }
public function load_data(Request $request){
        if($request->ajax()){
           if($request->last_record>0){
              $product_category= Category::find($request->category)->Clothes()->Where('id','<',$request->last_record)->OrderBy('id','desc')->limit(4)->get();
              return View('pages.category.category_product_list')->with('product_category',$product_category)->render();
           }
        }
}

     /*public function layout_product_grid(Request $request){
      
      $idCate = $request->idCate;
      $product_category= Category::find($idCate)->Clothes()->paginate(5);
      $output ="";
      foreach($product_category as $value){
        $output .= '
        <div class="col-md-4 col-sm-6">
                            <div class="products">
                                <div class="thumbnail">
                                    <a href="details.html">
                                        <img src="images/products/'.$value["image"].'" alt="Product Name">
                                    </a>
                                </div>
                                <div class="productname">'.
                                   $value["name"]
                                .'</div>
                                <h4 class="price">'.
                                $value["name"]
                                .'</h4>
                                <div class="button_group">
                                    <button class="button add-cart" type="button">
                                        Add To Cart
                                    </button>
                                    <button class="button compare" type="button">
                                        <i class="fa fa-exchange">
                                        </i>
                                    </button>
                                    <button class="button wishlist" type="button">
                                        <i class="fa fa-heart-o">
                                        </i>
                                    </button>
                                </div>
                            </div>
                        </div>';
      }
      echo $output;
   
     }*/
}