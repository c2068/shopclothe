<?php

namespace App\Listeners;
use Session;
use Auth;
use App\Events\Logined;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Cart;
class AddCartSession
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Logined  $event
     * @return void
     */
    public function handle(Logined $event)
    {
        if (Session::get('cart')){
            foreach(Session::get('cart') as $value){
                $check=Cart::Where('idUser',Auth::user()->id)->Where('idClothe',$value["id"])->first(); 
                if($check){
                    $quanity= $check->quanity;
                    $check->quanity = $quanity+$value["quanity"];
                    $check->total = $check->quanity* $check->price;
                    $check->save();
                }
                else{
                    $cart = new Cart();
                    $cart->idUser= Session::get('idUser');
                    $cart->idClothe= $value["id"];
                    $cart->name= $value["name"];
                    $cart->image= $value["image"];
                    $cart->price= $value["price"];
                    $cart->quanity= $value["quanity"];
                    $cart->total = $value["price"]*$value["quanity"];
                    $cart->save();
                }
               
            }
           Session::put('cart',null);
           }   
    }
}
