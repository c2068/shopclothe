<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_detail', function (Blueprint $table) {
            $table->increments("id");
            $table->integer("comment_id");
            $table->integer("customer_id");
            $table->string("customer_name");
            $table->string("customer_image");
            $table->string('content');
            $table->integer("like");
            $table->integer("dislike");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment_detail');
    }
}
