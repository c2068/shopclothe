<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatecommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratecomments', function (Blueprint $table) {
            $table->unsignedInteger("comment_id");
            $table->unsignedBigInteger("user_id");
            $table->boolean('liked');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->foreign('comment_id')->references('id')->on('comment')->onDelete('cascade');;
            $table->primary(['comment_id','user_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratecomments');
    }
}
