<?php

namespace Database\Factories;

use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClotheFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
       
        return [
            'name' => $this->faker->unique()->words($nb=2,$asText = True),
            'image' => 'clothe_'.$this->faker->unique()->numberBetween(0,138).'.jpg',

            'short_description'=>$this->faker->text(200),
            'description' => $this->faker->text(500),
            'regular_price' =>$this->faker->numberBetween(10,500),
            'SKU' =>'DIGI'.$this->faker->unique()->numberBetween(100,500),
            'stock_status' =>'instock',
            'quanity' => $this->faker->numberBetween(100,200),
            'category_id' =>$this->faker->numberBetween(1,5)
        ];
    }
}
