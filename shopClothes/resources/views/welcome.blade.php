<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="images/favicon.png">
    <title>Welcome to FlatShop</title>
    <base href="{{asset('public/FrontEnd/')}}/">
    <link href="{{asset('public/FrontEnd/css/bootstrap.css')}}" rel="stylesheet">
    <link
        href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,700,500italic,100italic,100'
        rel='stylesheet' type='text/css'>

    <link href="{{asset('public/FrontEnd/css/font-awesome.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/FrontEnd/css/flexslider.css')}}" type="text/css" media="screen" />
    <link href="{{asset('public/FrontEnd/css/sequence-looptheme.css')}}" rel="stylesheet" media="all" />
    <link href="{{asset('public/FrontEnd/css/new.css')}}" rel="stylesheet">

    <link href="{{asset('public/FrontEnd/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('public/FrontEnd/css/sweetalert.css')}}" rel="stylesheet">
    <!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script><script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script><![endif]-->
@stack('styles')
</head>

<body id="home">
    <div class="wrapper">
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 col-sm-2">
                        <div class="logo"><a href="{{URL::to('/')}}"><img src="images/logo.png" alt="FlatShop"></a>
                        </div>
                    </div>
                    <div class="col-md-10 col-sm-10">
                        <div class="header_top">
                            <div class="row">
                                <div class="col-md-3">
                                    <ul class="option_nav">
                                        <li class="dorpdown">
                                            <a href="#">Eng</a>
                                            <ul class="subnav">
                                                <li><a href="#">Eng</a></li>
                                                <li><a href="#">Vns</a></li>
                                                <li><a href="#">Fer</a></li>
                                                <li><a href="#">Gem</a></li>
                                            </ul>
                                        </li>
                                        <li class="dorpdown">
                                            <a href="#">USD</a>
                                            <ul class="subnav">
                                                <li><a href="#">USD</a></li>
                                                <li><a href="#">UKD</a></li>
                                                <li><a href="#">FER</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="topmenu">
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">News</a></li>
                                        <li><a href="#">Service</a></li>
                                        <li><a href="#">Recruiment</a></li>
                                        <li><a href="#">Media</a></li>
                                        <li><a href="#">Support</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-3">
                                    <?php 
                                       if(!Session::has("idUser")){?>
                                    <ul class="usermenu">
                                        <li><a href="{{route('login')}}" class="log">login</a></li>
                                        <li><a href="{{url('/logout')}}" class="reg">Register</a></li>
                                    </ul>
                                    <?php }
                                    else{?>
                                    <ul class="usermenu">

                                        <li>

                                            <a data-bs-toggle="dropdown"
                                                style="cursor: context-menu;display: flex; align-items: center">
                                                <img src="./images/user/{{Session::get('imageUser')}}"
                                                    style="width:20px; height: 20px; border-radius: 50%" alt="">
                                                <!--{{Session::get("name")}}-->
                                                {{Auth::user()->name}}
                                            </a>
                                            <ul class="dropdown-menu" style="width: 50px">
                                                <li style="width: 100%"><a class="dropdown-item" href="#">Tài khoản</a>
                                                </li>
                                                <li style="width: 100%"><a class="dropdown-item"
                                                        href="{{url('/user/purchase')}}">Đơn mua</a>
                                                </li>
                                                <li style="width: 100%"><a href="{{url('/logout')}}" class="log">Đăng
                                                        xuất</a></li>
                                            </ul>
                                        </li>
                                        <!--li>
                                            <a data-bs-toggle="dropdown" class="log" style="cursor: context-menu;">
                                                New
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="#">Actionvbnvbnvbnvbnvbnvbnbvn</a>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Another
                                                        actionvbnvbnvbnvbnvbnbvnbvn</a></li>
                                                <li><a class="dropdown-item" href="#">Something else here</a></li>
                                            </ul>
                                        </li-->
                                        <li class="nav-item dropdown no-arrow mx-1">
                                            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown"
                                                role="button" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                <i class="fa fa-bell-o" aria-hidden="true" style="font-size: 15px"></i>
                                                <span class="badge badge-danger badge-counter"
                                                    style="background: red; padding: 2px 5px; position: absolute; top: -6px; left: 30px">3+</span>
                                            </a>
                                            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                                aria-labelledby="alertsDropdown"
                                                style="width: 320px; top: 5px;background: #f9c3b8">
                                                <h6 class="dropdown-header"
                                                    style="height:33px; font-size:15px; font-weight: 400; color: white">
                                                    Thông báo
                                                </h6>
                                                <div id="notify">
                                                    <?php for($i=1;$i<=3;$i++){?>
                                                    <a class="dropdown-item d-flex align-items-center notify" href="#"
                                                        style="display:flex;width:100%; padding: 8px;">
                                                        <div class="mr-3" style="width: 65px; height:60px">
                                                            <img src="images/products/Vay_XepLi.jpg" alt="">
                                                        </div>
                                                        <div style="margin-left:8px">
                                                            <div class="small text-gray-500" style="font-weight: 400">
                                                                December 12, 2019</div>
                                                            <span class="noti font-weight-bold"
                                                                style=" font-size: 0.80rem; font-weight:400">A new
                                                                monthly report is ready
                                                                to
                                                                download!</span>
                                                        </div>
                                                        <div class="status"
                                                            style="width: 10px; height: 10px; border-radius: 5px">

                                                        </div>
                                                    </a>
                                                    <?php } ?>

                                                </div>

                                                <a class="dropdown-item text-center small text-gray-500" href="#"
                                                    style="width: 100%; height: 30px; line-height: 30px">Xem tất cả</a>
                                            </div>

                                        </li>
                                    </ul>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="header_bottom">
                            <ul class="option">
                                <li id="search" class="search">
                                    <form><input class="search-submit" type="submit" value="">
                                        <input class="search-input" placeholder="Enter your search term..." type="text"
                                            value="" name="search">
                                        <div id="search_ajax"
                                            style="position: absolute; top: 68px; left: -188px; width: 221px">
                                        </div>
                                    </form>

                                </li>
                                <?php 
                                   if(Session::has("idUser")){?>
                                @include('pages.cart-icon.cart_user')
                                <?php }
                                   else{?>
                                @include('pages.cart-icon.cart_session')
                                <?php } ?>
                            </ul>
                            <div class="navbar-header"><button type="button" class="navbar-toggle"
                                    data-toggle="collapse" data-target=".navbar-collapse"><span class="sr-only">Toggle
                                        navigation</span><span class="icon-bar"></span><span
                                        class="icon-bar"></span><span class="icon-bar"></span></button></div>
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li class="active dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Home</a>
                                        <div class="dropdown-menu">
                                            <ul class="mega-menu-links">
                                                <li><a href="index.html">home</a></li>
                                                <li><a href="home2.html">home2</a></li>
                                                <li><a href="home3.html">home3</a></li>
                                                <li><a href="productlitst.html">Productlitst</a></li>
                                                <li><a href="productgird.html">Productgird</a></li>
                                                <li><a href="details.html">Details</a></li>
                                                <li><a href="cart.html">Cart</a></li>
                                                <li><a href="checkout.html">CheckOut</a></li>
                                                <li><a href="checkout2.html">CheckOut2</a></li>
                                                <li><a href="contact.html">contact</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li><a href="{{url('/category/2')}}">men</a></li>
                                    <li><a href="{{url('/category/1')}}">women</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Fashion</a>
                                        <div class="dropdown-menu mega-menu">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6">
                                                    <ul class="mega-menu-links">
                                                        <li><a href="productgird.html">New Collection</a></li>
                                                        <li><a href="productgird.html">Shirts & tops</a></li>
                                                        <li><a href="productgird.html">Laptop & Brie</a></li>
                                                        <li><a href="productgird.html">Dresses</a></li>
                                                        <li><a href="productgird.html">Blazers & Jackets</a></li>
                                                        <li><a href="productgird.html">Shoulder Bags</a></li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-6 col-sm-6">
                                                    <ul class="mega-menu-links">
                                                        <li><a href="productgird.html">New Collection</a></li>
                                                        <li><a href="productgird.html">Shirts & tops</a></li>
                                                        <li><a href="productgird.html">Laptop & Brie</a></li>
                                                        <li><a href="productgird.html">Dresses</a></li>
                                                        <li><a href="productgird.html">Blazers & Jackets</a></li>
                                                        <li><a href="productgird.html">Shoulder Bags</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li><a href="productgird.html">gift</a></li>
                                    <li><a href="productgird.html">kids</a></li>
                                    <li><a href="productgird.html">blog</a></li>
                                    <li><a href="productgird.html">jewelry</a></li>
                                    <li><a href="contact.html">contact us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        @yield('content')
        <div class="clearfix"></div>
        @yield('content1')
        <div class="clearfix"></div>
        <div class="footer">
            <div class="footer-info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="footer-logo"><a href="#"><img src="images/logo.png" alt=""></a></div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <h4 class="title">Contact <strong>Info</strong></h4>
                            <p>No. 08, Nguyen Trai, Hanoi , Vietnam</p>
                            <p>Call Us : (084) 1900 1008</p>
                            <p>Email : michael@leebros.us</p>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <h4 class="title">Customer<strong> Support</strong></h4>
                            <ul class="support">
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Payment Option</a></li>
                                <li><a href="#">Booking Tips</a></li>
                                <li><a href="#">Infomation</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h4 class="title">Get Our <strong>Newsletter </strong></h4>
                            <p>Lorem ipsum dolor ipsum dolor.</p>
                            <form class="newsletter">
                                <input type="text" name="" placeholder="Type your email....">
                                <input type="submit" value="SignUp" class="button">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright-info">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p>Copyright © 2012. Designed by <a href="#">Michael Lee</a>. All rights reseved</p>
                        </div>
                        <div class="col-md-6">
                            <ul class="social-icon">
                                <li><a href="#" class="linkedin"></a></li>
                                <li><a href="#" class="google-plus"></a></li>
                                <li><a href="#" class="twitter"></a></li>
                                <li><a href="#" class="facebook"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JavaScript==================================================-->
    <script type="text/javascript" src="{{asset('public/FrontEnd/js/jquery-1.10.2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/FrontEnd/js/bootstrap.min.js')}}"></script>
    <script defer src="{{asset('public/FrontEnd/js/jquery.flexslider.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/FrontEnd/js/jquery.carouFredSel-6.2.1-packed.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/FrontEnd/js/jquery.elevatezoom.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/FrontEnd/js/script.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/FrontEnd/js/jquery.easing.1.3.js')}}"></script>
    <script type="text/javascript" src="{{asset('public/FrontEnd/js/jquery.sequence-min.js')}}"></script>
    <script src="{{asset('public/FrontEnd/js/sweetalert.js')}}"></script>
    <script src="{{asset('public/FrontEnd/js/simple.money.format.js')}}"></script>
    <script src="{{asset('public/FrontEnd/js/newJQery.js')}}"></script>
    <script src="{{asset('public/FrontEnd/js/bootstrap.bundle.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    @stack('scripts')
    <script type="text/javascript">
    var $ = jQuery.noConflict();
    $(document).ready(function() {
        $(document).on('click', '.add-cart', function() {
            var id = $(this).data("id");
            var name = $('.name_' + id).val();
            var image = $('.image_' + id).val();
            var price = $('.price_' + id).val();
            $.ajax({
                url: "{{URL::to('add-cart-ajax')}}",
                method: "POST",
                data: {
                    id: id,
                    name: name,
                    image: image,
                    price: price,
                    _token: '{!! csrf_token() !!}'
                },
                success: function(data) {
                    swal({
                            title: "Thêm Giỏ hàng thành công!",
                            text: "Bạn có muốn đến giỏ hàng!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Xem giỏ hàng!",
                            closeOnConfirm: false
                        },
                        function() {
                            window.location.href = "{{url('/show-cart-ajax')}}";
                        });

                }
            });
        });
        /*$('.add-cart').click(function() {
            var id = $(this).data("id");
            var name = $('.name_' + id).val();
            var image = $('.image_' + id).val();
            var price = $('.price_' + id).val();
            $.ajax({
                url: "{{URL::to('add-cart-ajax')}}",
                method: "POST",
                data: {
                    id: id,
                    name: name,
                    image: image,
                    price: price,
                    _token: '{!! csrf_token() !!}'
                },
                success: function(data) {
                    swal({
                            title: "Thêm Giỏ hàng thành công!",
                            text: "Bạn có muốn đến giỏ hàng!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "Xem giỏ hàng!",
                            closeOnConfirm: false
                        },
                        function() {
                            window.location.href = "{{url('/show-cart-ajax')}}";
                        });

                }
            });
        });*/
      
        $('.search-input').keyup(function() {
            var search = $(this).val();
            if (search != "") {
                $.ajax({
                    url: "{{URL::to('/search-ajax')}}",
                    data: {
                        search: search,
                        _token: '{!! csrf_token() !!}'
                    },
                    method: "POST",
                    success: function(data) {
                        $('#search_ajax').fadeIn();
                        $('#search_ajax').html(data);
                        console.log(data);
                    }
                })

            } else {
                $('#search_ajax').fadeOut();
            }
        })
        $('.search-submit').mouseout(function() {
            $('#search_ajax').fadeOut();
        })

    }) 
    </script>
</body>

</html>