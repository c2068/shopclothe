@if(empty($order_code))
<div class="_2WXGP9">
    <div class="ground">
    <div class="_379iT1"></div>
    <div class="_1FBD3g">Chưa có đơn hàng</div>
    </div>
   
</div>
@else
@foreach($order_code as $value)
<?php $total = 0;?>
<div class="_32cw_C">
    <div>
        <div class="_3wGW0m">
            <div class="uhK5zO">
                <i class="fa fa-heart" aria-hidden="true"></i>

                <div style="font-weight:400">ĐÃ HỦY</div>
            </div>
            <div class="_2U1-Hf"></div>
            @foreach($order as $value1)
            @if($value["code"] == $value1->order_code)
            <?php $total += $value1->clothe_price* $value1->clothe_quanity;?>
            <a href="">
                <div class="_2hnmH-">
                    <div class="img">
                        <img src="images/products/{{$value1->clothe_image}}" alt="">
                    </div>
                    <div class="info_clothe">
                        <div class="name_clothe">[Video+Ảnh Thật] {{$value1->clothe_name}}</div>
                        <div class="color_clothe">Phân loại hàng: Đen</div>
                        <div class="quanity_clothe">x{{$value1->clothe_quanity}}</div>
                    </div>
                    <div class="price_clothe">{{number_format($value1->clothe_price)}} đ</div>
                </div>
            </a>
            <div class="_2U1-Hf"></div>
            @endif
            @endforeach
        </div>
    </div>
    <div class="_2U1-Hf"></div>
    <div class="_39ypT2">
        <div class="h0Jexl">
            <span>
                <svg width="16" height="17" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M15.94 1.664s.492 5.81-1.35 9.548c0 0-.786 1.42-1.948 2.322 0 0-1.644 1.256-4.642 2.561V0s2.892 1.813 7.94 1.664zm-15.88 0C5.107 1.813 8 0 8 0v16.095c-2.998-1.305-4.642-2.56-4.642-2.56-1.162-.903-1.947-2.323-1.947-2.323C-.432 7.474.059 1.664.059 1.664z"
                        fill="url(#paint0_linear)"></path>
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M8.073 6.905s-1.09-.414-.735-1.293c0 0 .255-.633 1.06-.348l4.84 2.55c.374-2.013.286-4.009.286-4.009-3.514.093-5.527-1.21-5.527-1.21s-2.01 1.306-5.521 1.213c0 0-.06 1.352.127 2.955l5.023 2.59s1.09.42.693 1.213c0 0-.285.572-1.09.28L2.928 8.593c.126.502.285.99.488 1.43 0 0 .456.922 1.233 1.56 0 0 1.264 1.126 3.348 1.941 2.087-.813 3.352-1.963 3.352-1.963.785-.66 1.235-1.556 1.235-1.556a6.99 6.99 0 00.252-.632L8.073 6.905z"
                        fill="#FEFEFE"></path>
                    <defs>
                        <linearGradient id="paint0_linear" x1="8" y1="0" x2="8" y2="16.095"
                            gradientUnits="userSpaceOnUse">
                            <stop stop-color="#F53D2D"></stop>
                            <stop offset="1" stop-color="#F63"></stop>
                        </linearGradient>
                    </defs>
                </svg>
            </span>
            <div class="_3LfHXA">Tổng số tiền:</div>
            <div class="isoXOF"><?php echo number_format($total - $value["feeship"])." VNĐ"?></div>
        </div>
    </div>
    @if($type ==1)
    <div class="_2GvoV8">
        <div class="sCoiFR">         
            <div class="EoE22E">
                <button class="buy Cancel_order" data-order_code="{{$value['code']}}">Hủy đơn</button>
            </div>
            <div class="Vt1J92"> <button class="" disabled>Chờ duyệt</button></div>
        </div>
    </div>
    @elseif(isset($type) &&  $type ==2)
    <div class="_2GvoV8">
        <div class="sCoiFR">
            <div class="EoE22E">
                <button class="buy">Mua lần nữa</button>
            </div>
            <div class="Vt1J92"><button class="" >Chi tiết  Hủy đơn</button></div>
            <div class="Vt1J92"><button class="detailcancel" data-order_code="{{$value['code']}}" >Chi tiết Hủy đơn</button></div>
        </div>
    </div>
    @endif
</div>
@endforeach
@endif
@push('styles')
  <style >
      ._2WXGP9{
      position: relative;
      }
.ground{
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
}   
    ._379iT1{
      margin: 0 auto;}
._1FBD3g{
    text-align: center;
}
  </style>
@endpush
@push('scripts')
<script type="text/javascript">
    var $ = jQuery.noConflict();

       $('div.purchase a').click(function(e) {
        e.preventDefault();

        $('div.purchase a').removeClass('_228ytk');
        $(this).addClass('_228ytk');
        var url = $(this).attr('href');
        history.pushState({}, null, url);
        $.ajax({
            url: url,
            data: {
                id: 1
            },
            success: function(data) {

                $('#show').html(data);
            }
        })
    })
    $(document).on('click', '.Cancel_order', function() {
        var order_code = $(this).data("order_code");
        $.ajax({
            url: "{{url('/cancel-order')}}",
            method: "post",
            data: {
                order_code: order_code,
                _token: '{!! csrf_token() !!}'
            },
            success: function(data) {
                if (data == 1) {
                    swal("Không thể xoas!");
                } else {
                    swal("Hủy đơn thành công!");
                    $('#show').html(data);
                }
            }
        })
    })
    $(document).on('click', '.detailcancel', function() {
        var order_code = $(this).data("order_code");

        history.pushState({}, null, 'http://localhost:8080/shopClothes/user/purchase/order/' + order_code +
            '?type=2');
        $.ajax({
            url: "{{url('/detailcancel')}}",
            method: "post",
            data: {
                order_code: order_code,
                _token: '{!! csrf_token() !!}'
            },
            success: function(data) {
                document.getElementById("topNav").style.display = "none";
                $('#show').html(data);
            }
        })
    })

    $(document).on('click', '._3JT7RU', function() {
        var url = "http://localhost:8080/shopClothes/user/purchase?type=2"
        history.pushState({}, null, url);
        $.ajax({
            url: url,
            data: {
                id: 1
            },
            success: function(data) {
                document.getElementById("topNav").style.display = "flex";

                $('#show').html(data);
            }
        })
    })
</script>
@endpush