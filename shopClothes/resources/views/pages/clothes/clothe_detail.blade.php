@extends('welcome')
@section('content')
<div class="container_fullwidth">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="products-details">
                    <div class="preview_image">
                        <div class="preview-small">
                            <img id="zoom_03" src="images/products/{{$clothe->image}}"
                                data-zoom-image="images/products/{{$clothe->image}}" alt="">
                        </div>
                        <div class="thum-image">
                            <ul id="gallery_01" class="prev-thum">
                                @foreach($image as $value)
                                <li>
                                    <a class="imageProduct" data-img="{{$value->image}}" id="">
                                        <img src="images/products/{{$value->image}}" alt="">
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                            <a class="control-left" id="thum-prev" href="javascript:void(0);">
                                <i class="fa fa-chevron-left">
                                </i>
                            </a>
                            <a class="control-right" id="thum-next" href="javascript:void(0);">
                                <i class="fa fa-chevron-right">
                                </i>
                            </a>
                        </div>
                    </div>
                    <form action="">
                        <div class="products-description">
                            <h5 class="name">
                                {{$clothe->name}}
                            </h5>
                            <input type="hidden" class="nameClothe" value="{{$clothe->name}}">
                            <p>
                                <img alt="" src="images/star.png">
                                <a class="review_num" href="#">
                                    02 Review(s)
                                </a>
                            </p>
                            <p>
                                Availability:
                                <span class=" light-red">
                                    In Stock
                                </span>
                            </p>
                            <hr class="border">
                            <div class="price">
                                Price :
                                <span class="new_price">
                                    {{number_format($clothe->price)}}
                                    <sup>
                                        $
                                    </sup>
                                </span>
                                <span class="old_price">
                                    450.00
                                    <sup>
                                        $
                                    </sup>
                                </span>
                                <input type="hidden" class="priceClothe" value="{{$clothe->price}}">

                            </div>
                            <hr class="border">
                            <div class="color">
                                Color:
                                <?php $qty =0;?>
                                @foreach($color as $value)
                                <?php $qty += $value->quanity ?>
                                @if($value->quanity>0)
                                <span><input type="button" class="btn-color" value="{{$value->color}}"
                                        data-id="{{$value->id}}" style="border: solid 0.5px #d8d8d8"></span>
                                @else
                                <span><input type="button" class="btn-color" value="{{$value->color}}"
                                        data-id="{{$value->id}}" style="border: solid 0.5px #d8d8d8" disabled></span>
                                @endif
                                @endforeach
                            </div>
                            <hr class="border">
                            <div class="quanity">
                                Quanity Total: <?php echo $qty?>
                            </div>

                            <hr class="border">
                            <div class="wided" style="margin-bottom: 15px">
                                <div class="qty" style="width:30%">
                                    Qty &nbsp;&nbsp;:
                                    <input type="number" id="quanityOrder" onchange="changeQuanity(this.value)"
                                        style="width: 55%" value=1>
                                </div>
                                <div class="button_group">
                                    <button class="button addCart" type="submit">
                                        Add To Cart
                                    </button>
                                    <button class="button compare">
                                        <i class="fa fa-exchange">
                                        </i>
                                    </button>
                                    <button class="button favorite">
                                        <i class="fa fa-heart-o">
                                        </i>
                                    </button>
                                    <button class="button favorite">
                                        <i class="fa fa-envelope-o">
                                        </i>
                                    </button>
                                </div>
                            </div>

                            <strong class="red" style="font-size: 14px"></strong>



                        </div>

                    </form>
                    <div class="clearfix">
                    </div>
                    <hr class="border">
                    <img src="images/share.png" alt="" class="pull-right">
                </div>
            </div>
            <div class="clearfix">
            </div>
            <div class="tab-box">
                <div id="tabnav">
                    <ul>
                        <li>
                            <a href="#Descraption">
                                DESCRIPTION
                            </a>
                        </li>
                        <li>
                            <a href="#Reviews">
                                REVIEW
                            </a>
                        </li>
                        <li>
                            <a href="#tags">
                                PRODUCT TAGS
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content-wrap">
                    <div class="tab-content" id="Descraption">
                        <p>
                            Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc.
                            Vestibulum ante ipsum primis in faucibus orci luctus et ultri ces posuere cubilia curae
                            Aenean eleifend laoreet congue. Proin lectus ipsum, gravida et mattis vulputate, tristique
                            ut lectus. Sed et lorem nunc. Vestibu um ante ipsum primis in faucibus orci luctus et ultri
                            ces posuere cubilia curae Aenean eleifend laoreet congue. Proin lectus ipsum, gravida et
                            mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in
                            faucibus orci luctus et ultri ces posuere cubilia curae Aenean eleifend laoreet congue.
                            Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc.
                            Vestibulum ante ipsum primis in faucibus orci luctus et ultri ces posuere cubilia curae...
                        </p>
                        <p>
                            Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc.
                            Vestibulum ante ipsum primis in faucibus orci luctus et ultri ces posuere cubilia curae
                            Aenean eleifend laoreet congue. Proin lectus ipsum, gravida et mattis vulputate, tristique
                            ut lectus. Sed et lorem nunc. Vestibu um ante ipsum primis in faucibus orci luctus et ultri
                            ces posuere cubilia curae Aenean eleifend laoreet congue. Proin lectus ipsum, gravida et
                            mattis vulputate, tristique ut lectus. Sed et lorem nunc...
                        </p>
                    </div>
                    <div class="tab-content" id="Reviews">
                        @include('pages.comment.show_comment')

                    </div>
                    <div class="tab-content">
                        <div class="review">
                            <p class="rating">
                                <i class="fa fa-star light-red">
                                </i>
                                <i class="fa fa-star light-red">
                                </i>
                                <i class="fa fa-star light-red">
                                </i>
                                <i class="fa fa-star-half-o gray">
                                </i>
                                <i class="fa fa-star-o gray">
                                </i>
                            </p>
                            <h5 class="reviewer">
                                Reviewer name
                            </h5>
                            <p class="review-date">
                                Date: 01-01-2014
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a eros neque. In sapien
                                est, malesuada non interdum id, cursus vel neque.
                            </p>
                        </div>
                        <div class="review">
                            <p class="rating">
                                <i class="fa fa-star light-red">
                                </i>
                                <i class="fa fa-star light-red">
                                </i>
                                <i class="fa fa-star light-red">
                                </i>
                                <i class="fa fa-star-half-o gray">
                                </i>
                                <i class="fa fa-star-o gray">
                                </i>
                            </p>
                            <h5 class="reviewer">
                                Reviewer name
                            </h5>
                            <p class="review-date">
                                Date: 01-01-2014
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer a eros neque. In sapien
                                est, malesuada non interdum id, cursus vel neque.
                            </p>
                        </div>
                    </div>
                    <div class="tab-content" id="tags">
                        <div class="tag">
                            Add Tags :
                            <input type="text" name="">
                            <input type="submit" value="Tag">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
            </div>
            <div id="productsDetails" class="hot-products">
                <h3 class="title">
                    <strong>
                        Hot
                    </strong>
                    Products
                </h3>
                <div class="control">
                    <a id="prev_hot" class="prev" href="#">
                        &lt;
                    </a>
                    <a id="next_hot" class="next" href="#">
                        &gt;
                    </a>
                </div>
                <ul id="hot">
                    <li>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="products">
                                    <div class="offer">
                                        - %20
                                    </div>
                                    <div class="thumbnail">
                                        <img src="images/products/small/products-01.png" alt="Product Name">
                                    </div>
                                    <div class="productname">
                                        Iphone 5s Gold 32 Gb 2013
                                    </div>
                                    <h4 class="price">
                                        $451.00
                                    </h4>
                                    <div class="button_group">
                                        <button class="button add-cart" type="button">
                                            Add To Cart
                                        </button>
                                        <button class="button compare" type="button">
                                            <i class="fa fa-exchange">
                                            </i>
                                        </button>
                                        <button class="button wishlist" type="button">
                                            <i class="fa fa-heart-o">
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="products">
                                    <div class="thumbnail">
                                        <img src="images/products/small/products-02.png" alt="Product Name">
                                    </div>
                                    <div class="productname">
                                        Iphone 5s Gold 32 Gb 2013
                                    </div>
                                    <h4 class="price">
                                        $451.00
                                    </h4>
                                    <div class="button_group">
                                        <button class="button add-cart" type="button">
                                            Add To Cart
                                        </button>
                                        <button class="button compare" type="button">
                                            <i class="fa fa-exchange">
                                            </i>
                                        </button>
                                        <button class="button wishlist" type="button">
                                            <i class="fa fa-heart-o">
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="products">
                                    <div class="offer">
                                        New
                                    </div>
                                    <div class="thumbnail">
                                        <img src="images/products/small/products-03.png" alt="Product Name">
                                    </div>
                                    <div class="productname">
                                        Iphone 5s Gold 32 Gb 2013
                                    </div>
                                    <h4 class="price">
                                        $451.00
                                    </h4>
                                    <div class="button_group">
                                        <button class="button add-cart" type="button">
                                            Add To Cart
                                        </button>
                                        <button class="button compare" type="button">
                                            <i class="fa fa-exchange">
                                            </i>
                                        </button>
                                        <button class="button wishlist" type="button">
                                            <i class="fa fa-heart-o">
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="products">
                                    <div class="offer">
                                        - %20
                                    </div>
                                    <div class="thumbnail">
                                        <img src="images/products/small/products-01.png" alt="Product Name">
                                    </div>
                                    <div class="productname">
                                        Iphone 5s Gold 32 Gb 2013
                                    </div>
                                    <h4 class="price">
                                        $451.00
                                    </h4>
                                    <div class="button_group">
                                        <button class="button add-cart" type="button">
                                            Add To Cart
                                        </button>
                                        <button class="button compare" type="button">
                                            <i class="fa fa-exchange">
                                            </i>
                                        </button>
                                        <button class="button wishlist" type="button">
                                            <i class="fa fa-heart-o">
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="products">
                                    <div class="thumbnail">
                                        <img src="images/products/small/products-02.png" alt="Product Name">
                                    </div>
                                    <div class="productname">
                                        Iphone 5s Gold 32 Gb 2013
                                    </div>
                                    <h4 class="price">
                                        $451.00
                                    </h4>
                                    <div class="button_group">
                                        <button class="button add-cart" type="button">
                                            Add To Cart
                                        </button>
                                        <button class="button compare" type="button">
                                            <i class="fa fa-exchange">
                                            </i>
                                        </button>
                                        <button class="button wishlist" type="button">
                                            <i class="fa fa-heart-o">
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="products">
                                    <div class="offer">
                                        New
                                    </div>
                                    <div class="thumbnail">
                                        <img src="images/products/small/products-03.png" alt="Product Name">
                                    </div>
                                    <div class="productname">
                                        Iphone 5s Gold 32 Gb 2013
                                    </div>
                                    <h4 class="price">
                                        $451.00
                                    </h4>
                                    <div class="button_group">
                                        <button class="button add-cart" type="button">
                                            Add To Cart
                                        </button>
                                        <button class="button compare" type="button">
                                            <i class="fa fa-exchange">
                                            </i>
                                        </button>
                                        <button class="button wishlist" type="button">
                                            <i class="fa fa-heart-o">
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="row">
                            <div class="col-md-4 col-sm-4">
                                <div class="products">
                                    <div class="offer">
                                        - %20
                                    </div>
                                    <div class="thumbnail">
                                        <img src="images/products/small/products-01.png" alt="Product Name">
                                    </div>
                                    <div class="productname">
                                        Iphone 5s Gold 32 Gb 2013
                                    </div>
                                    <h4 class="price">
                                        $451.00
                                    </h4>
                                    <div class="button_group">
                                        <button class="button add-cart" type="button">
                                            Add To Cart
                                        </button>
                                        <button class="button compare" type="button">
                                            <i class="fa fa-exchange">
                                            </i>
                                        </button>
                                        <button class="button wishlist" type="button">
                                            <i class="fa fa-heart-o">
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="products">
                                    <div class="thumbnail">
                                        <img src="images/products/small/products-02.png" alt="Product Name">
                                    </div>
                                    <div class="productname">
                                        Iphone 5s Gold 32 Gb 2013
                                    </div>
                                    <h4 class="price">
                                        $451.00
                                    </h4>
                                    <div class="button_group">
                                        <button class="button add-cart" type="button">
                                            Add To Cart
                                        </button>
                                        <button class="button compare" type="button">
                                            <i class="fa fa-exchange">
                                            </i>
                                        </button>
                                        <button class="button wishlist" type="button">
                                            <i class="fa fa-heart-o">
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="products">
                                    <div class="offer">
                                        New
                                    </div>
                                    <div class="thumbnail">
                                        <img src="images/products/small/products-03.png" alt="Product Name">
                                    </div>
                                    <div class="productname">
                                        Iphone 5s Gold 32 Gb 2013
                                    </div>
                                    <h4 class="price">
                                        $451.00
                                    </h4>
                                    <div class="button_group">
                                        <button class="button add-cart" type="button">
                                            Add To Cart
                                        </button>
                                        <button class="button compare" type="button">
                                            <i class="fa fa-exchange">
                                            </i>
                                        </button>
                                        <button class="button wishlist" type="button">
                                            <i class="fa fa-heart-o">
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="col-md-3">
            <div class="special-deal leftbar">
                <h4 class="title">
                    Special
                    <strong>
                        Deals
                    </strong>
                </h4>
                <div class="special-item">
                    <div class="product-image">
                        <a href="#">
                            <img src="images/products/thum/products-01.png" alt="">
                        </a>
                    </div>
                    <div class="product-info">
                        <p>
                            Licoln Corner Unit
                        </p>
                        <h5 class="price">
                            $300.00
                        </h5>
                    </div>
                </div>
                <div class="special-item">
                    <div class="product-image">
                        <a href="#">
                            <img src="images/products/thum/products-02.png" alt="">
                        </a>
                    </div>
                    <div class="product-info">
                        <p>
                            Licoln Corner Unit
                        </p>
                        <h5 class="price">
                            $300.00
                        </h5>
                    </div>
                </div>
                <div class="special-item">
                    <div class="product-image">
                        <a href="#">
                            <img src="images/products/thum/products-03.png" alt="">
                        </a>
                    </div>
                    <div class="product-info">
                        <p>
                            Licoln Corner Unit
                        </p>
                        <h5 class="price">
                            $300.00
                        </h5>
                    </div>
                </div>
            </div>
            <div class="clearfix">
            </div>
            <div class="product-tag leftbar">
                <h3 class="title">
                    Products
                    <strong>
                        Tags
                    </strong>
                </h3>
                <ul>
                    <li>
                        <a href="#">
                            Lincoln us
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            SDress for Girl
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Corner
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Window
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            PG
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Oscar
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Bath room
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            PSD
                        </a>
                    </li>
                </ul>
            </div>
            <div class="clearfix">
            </div>
            <div class="get-newsletter leftbar">
                <h3 class="title">
                    Get
                    <strong>
                        newsletter
                    </strong>
                </h3>
                <p>
                    Casio G Shock Digital Dial Black.
                </p>
                <form>
                    <input class="email" type="text" name="" placeholder="Your Email...">
                    <input class="submit" type="submit" value="Submit">
                </form>
            </div>
            <div class="clearfix">
            </div>
            <div class="fbl-box leftbar">
                <h3 class="title">
                    Facebook
                </h3>
                <span class="likebutton">
                    <a href="#">
                        <img src="images/fblike.png" alt="">
                    </a>
                </span>
                <p>
                    12k people like Flat Shop.
                </p>
                <ul>
                    <li>
                        <a href="#">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        </a>
                    </li>
                </ul>
                <div class="fbplug">
                    <a href="#">
                        <span>
                            <img src="images/fbicon.png" alt="">
                        </span>
                        Facebook social plugin
                    </a>
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="our-brand">
        <h3 class="title">
            <strong>
                Our
            </strong>
            Brands
        </h3>
        <div class="control">
            <a id="prev_brand" class="prev" href="#">
                &lt;
            </a>
            <a id="next_brand" class="next" href="#">
                &gt;
            </a>
        </div>
        <ul id="braldLogo">
            <li>
                <ul class="brand_item">
                    <li>
                        <a href="#">
                            <div class="brand-logo">
                                <img src="images/envato.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="brand-logo">
                                <img src="images/themeforest.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="brand-logo">
                                <img src="images/photodune.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="brand-logo">
                                <img src="images/activeden.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="brand-logo">
                                <img src="images/envato.png" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <ul class="brand_item">
                    <li>
                        <a href="#">
                            <div class="brand-logo">
                                <img src="images/envato.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="brand-logo">
                                <img src="images/themeforest.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="brand-logo">
                                <img src="images/photodune.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="brand-logo">
                                <img src="images/activeden.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="brand-logo">
                                <img src="images/envato.png" alt="">
                            </div>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
var $ = jQuery.noConflict();
var imgClothe = "";

$('.addCart').click(function(e) {
    e.preventDefault();
    var idClothe = $('.orange').data("id");
    var name = $('.nameClothe').val();
    var price = $('.priceClothe').val();
    var image = imgClothe;
    var quanity = $('#quanityOrder').val();
    if (idClothe) {
        $.ajax({
            url: "{{URL::to('add-cart-ajax')}}",
            method: "POST",
            data: {
                id: idClothe,
                name: name,
                image: image,
                price: price,
                quanity: quanity,
                _token: '{!! csrf_token() !!}'
            },
            success: function(data) {
                swal({
                        title: "Thêm Giỏ hàng thành công!",
                        text: "Bạn có muốn đến giỏ hàng!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Xem giỏ hàng!",
                        closeOnConfirm: false
                    },
                    function() {
                        window.location.href = "{{url('/show-cart-ajax')}}";
                    });

            }
        });
    } else {
        swal('Vui lòng chọn màu sắc!')
    }
})
$('.btn-color').click(function() {
    $('input.btn-color').removeClass("orange");
    $(this).addClass('orange');
    var id_color = $(this).data("id");
    $('.red').html("");
    $.ajax({
        url: "{{url('/change-color')}}",
        method: "POST",
        data: {
            id_color: id_color,
            _token: '{!! csrf_token() !!}'
        },
        success: function(data) {
            document.getElementById("zoom_03").setAttribute("src",
                "images/products/" + data["image"]);
            document.getElementById("zoom_03").setAttribute("data-zoom-image",
                "images/products/" + data["image"]);
            $('.quanity').html("Quanity Total: " + data["quanity"]);
            quanity = data["quanity"];
            imgClothe = data["image"];
            $("#quanityOrder").val(1);

        }
    })
})

function changeQuanity($qty) {
    var idClothe = $('.orange').data("id");
    /* $.ajax({
         url: "{{URL::to('change-quanity')}}",
         method: "POST",
         data:{ id: idClothe, quanity: quanity,   _token: '{!! csrf_token() !!}' },
         success: function(data){

         }
     })*/
    if ($qty > quanity) {
        $("#quanityOrder").val(quanity);
        $('.red').html("Đã đạt đến số lượng mua tối đa cho phép của sản phẩm này.")
    } else {
        $('.red').html("")
    }
}

function typeComment() {
    var text = document.getElementById("textComment").value;
    if (text == '') {
        $('#Comment').removeClass("comment");
        document.getElementById("Comment").disabled = true;

    } else {
        $('#Comment').addClass("comment");
        document.getElementById("Comment").disabled = false;
    }
}
$('#Comment').click(function() {
    var text = document.getElementById("textComment").value;
    var id = $(this).data("id");
    $.ajax({
        url: "{{url('/add-comment')}}",
        method: "post",
        data: {
            id: id,
            content: text,
            _token: '{!! csrf_token() !!}'
        },
        success: function(data) {
            document.getElementById("textComment").value = "";
            $('#list-comment').html(data);
        }
    })
})
$('.imageProduct').hover(function() {
            var img = $(this).data("img");
            document.getElementById("zoom_03").setAttribute("src", "images/products/" + img);
            document.getElementById("zoom_03").setAttribute("data-zoom-image", "images/products/" +
                img);
        })
      
</script>
@endpush