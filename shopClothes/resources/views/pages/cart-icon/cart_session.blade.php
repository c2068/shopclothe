<li class="option-cart">
    <a href="#" class="cart-icon">cart <span class="cart_no">@if(Session::has('cart')) {{count(Session::get('cart'))}}
            @endif</span></a>
    <ul class="option-cart-item">
        @if(Session::has('cart'))
        <?php $total =0;?>
        @foreach(Session::get('cart') as $value)
        <?php $total += $value["quanity"]*$value["price"];?>
        <li>
            <div class="cart-item">
                <div class="image"><img src="images/products/{{$value['image']}}" alt=""></div>
                <div class="item-description">
                    <p class="name">{{$value['name']}}</p>
                    <p>Size: <span class="light-red">One size</span><br>Quantity: <span
                            class="light-red">{{$value['quanity']}}</span></p>
                </div>
                <div class="right">
                    <p class="price">{{$value['price']}}</p>
                    <a class="grab delete-product" style="cursor: -webkit-grab; cursor: grab;"
                        data-id="{{$value['id']}}">
                        <img src="images/remove.png" alt="">
                    </a>
                </div>
            </div>
        </li>
        @endforeach
        <li><span class="total">Total <strong><?php echo number_format($total);?></strong></span><button
                class="checkout" onClick="location.href='{{url('/show-cart-ajax')}}'">CheckOut</button></li>
        @endif
    </ul>
</li>