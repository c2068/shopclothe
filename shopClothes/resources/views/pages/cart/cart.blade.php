@extends('welcome')
@section('content')
<div class="container_fullwidth">
    <div class="container shopping-cart">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title">
                    Shopping Cart
                </h3>
                <div class="clearfix">
                </div>
                <table class="shop-table">
                    <thead>
                        <tr>
                            <th>
                                Image
                            </th>
                            <th>
                                Dtails
                            </th>
                            <th>
                                Price
                            </th>
                            <th>
                                Quantity
                            </th>
                            <th>
                                ToTal
                            </th>
                            <th>
                                Delete
                            </th>
                            <th>&nbsp;&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(Session::get("idUser"))
                        @include("pages.cart.cart_user")
                        @else
                        @include("pages.cart.cart_ajax")
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6">
                                <button class="pull-left shopping">
                                    Continue Shopping
                                </button>
                                <button class="pull-right vong">
                                    Update Shopping Cart
                                </button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                <div class="clearfix">
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="shippingbox">
                            <h5>
                                Estimate Shipping And Tax
                            </h5>

                            <label>
                                Select Country *
                            </label>
                            <select class="choose" name="city" id="city">
                                <option value="">-- Chọn Thành Phố --</option>
                                @foreach($city as $value)
                                <option value="{{$value->id}}">
                                    {{$value->namecity}}
                                </option>
                                @endforeach
                            </select>
                            <label>
                                State / Province *
                            </label>
                            <select class="choose" name="province" id="province">
                                <option value="">-- Chọn Quận/Huyện --</option>
                            </select>
                            <label>
                                Tiền Ship:
                                <span id="fee" style="color: #FE5252; font-weight: 500">0 VNĐ</span>
                            </label>

                            <div class="clearfix">
                            </div>
                            <button class="feeship_OK">
                                Agree
                            </button>

                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="shippingbox">
                            <h5>
                                Discount Codes
                            </h5>
                            <form>

                                <label>
                                    Enter your coupon code if you have one
                                </label>
                                <input type="text" class="coupon" name="coupon" disabled="disabled">
                                <div class="clearfix">
                                </div>
                                <button type="button" class="getCoupon">
                                    Get A Qoute
                                </button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="shippingbox">
                            <form action="{{URL::to('/checkout')}}" method="POST">
                                @csrf
                                <div class="subtotal">
                                    <h5>
                                        Sub Total
                                    </h5>
                                    <span id="subtotal">
                                        0 VNĐ
                                    </span>
                                </div>
                                <div class="feeship">
                                    <h5 class="ship" style="float: left;width:45%;font-weight:400;text-align: right">
                                    </h5>
                                    <span id="feeship"
                                        style="float:right;width:45%;font-size:20px;font-weight:300"></span>
                                </div>
                                <div class="subtotal coupon">
                                    <h5></h5>
                                    <span id="coupon"></span>
                                </div>
                                <div class="grandtotal">
                                    <h5>
                                        GRAND TOTAL
                                    </h5>

                                    <span id="grandtotal">
                                        0 VNĐ
                                    </span>

                                </div>
                                <input type="hidden" name="grandtotal" class="grandtotal_cart" value="linh">
                                <input type="hidden" name="city" class="city_cart">
                                <input type="hidden" name="province" class="province_cart">
                                <input type="hidden" name="feeship" class="feeship_cart">
                                <input type="hidden" name="clothe_select" class="clothe_select_cart">
                                <input type="hidden" name="coupon" class="coupon_cart">
                                <button type="submit" id="checkout" data-user="{{Session::get('idUser')}}">
                                    Process To Checkout
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
        </div>
        <div class="our-brand">
            <h3 class="title">
                <strong>
                    Our
                </strong>
                Brands
            </h3>
            <div class="control">
                <a id="prev_brand" class="prev" href="#">
                    &lt;
                </a>
                <a id="next_brand" class="next" href="#">
                    &gt;
                </a>
            </div>
            <ul id="braldLogo">
                <li>
                    <ul class="brand_item">
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/envato.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/themeforest.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/photodune.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/activeden.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/envato.png" alt="">
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <ul class="brand_item">
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/envato.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/themeforest.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/photodune.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/activeden.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/envato.png" alt="">
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    var $ = jQuery.noConflict();
    var product_id = [];
    var quanity = 0;
    var imgClothe = "";
    var subtotal = 0;
    var feeship = 0;
$(document).ready(function() {
    $('.select_product').click(function() {
        if ($(this).prop('checked') == true) {
            var id = $(this).data("id");
            product_id.push(id);

        } else {
            var id = $(this).data("id");
            for (var i = product_id.length - 1; i >= 0; i--) {
                if (product_id[i] === id) {
                    product_id.splice(i, 1);
                }
            }
        }
        $('.clothe_select_cart').val(product_id);
        if (product_id.length > 0) {
            $('.coupon').removeAttr('disabled');
            $.ajax({
                url: "{{URL::to('/select-product')}}",
                method: "POST",
                data: {
                    product_id: product_id,
                    _token: '{!! csrf_token() !!}'

                },
                success: function(data) {
                    /*if (data.length == 2) {
                        subtotal = data[0];
                        $('#subtotal').html(data[0]);
                        $('#grandtotal').html(data[1]);
                    } else {
                        subtotal = data;
                        $('#subtotal').html(data);
                        $('#grandtotal').html(data);
                    }*/

                    $('#subtotal').html(data).simpleMoneyFormat();
                    subtotal = parseInt(data);
                    var grandtotal = parseInt(data) + parseInt(feeship);
                    $('#grandtotal').html(grandtotal).simpleMoneyFormat();
                    $('.grandtotal_cart').val(grandtotal);
                }
            });
        } else {

            $('.coupon').attr({
                'disabled': 'disabled'
            });
            $('#subtotal').html("0 VNĐ");
            $('#grandtotal').html("0 VNĐ");

        }
    });
    $('.delete-product').click(function(event) {
        event.preventDefault();
        var id = $(this).data("id");
        $.ajax({
            url: "{{URL::to('/delete-cart-ajax')}}",
            method: "POST",
            data: {
                id: id,
                _token: '{!! csrf_token() !!}'
            },
            success: function(data) {
                location.reload();
            }
        })
    });
    $('.shopping').click(function() {
        location.href = "{{URL::to('/trang-chu')}}"
    })
    $('.choose').change(function() {
        var idType = $(this).attr("id");
        var id = $(this).val();
        if (idType == "city") {
            result = "province";
            $('#fee').html("0 VNĐ");
        } else {
            result = "fee";
        }
        $.ajax({
            url: "{{url('/select-delivery')}}",
            method: "POST",
            data: {
                id: id,
                idType: idType,
                _token: '{!! csrf_token() !!}'
            },
            success: function(data) {
                $('#' + result).html(data + " VNĐ");
                console.log(data);
            }
        })
    })
    $('.feeship_OK').click(function() {
        if (product_id.length == 0) {
            swal("Mời bạn chọn sản phẩm!")
        } else {
            var city = $('#city').val();
            var province = $('#province').val();
            if (province == "") {
                swal("Chọn quận huyện!");
            } else {
                $.ajax({
                    url: "{{url('/confirm-feeship')}}",
                    method: "POST",
                    data: {
                        product_id: product_id,
                        province: province,
                        _token: '{!! csrf_token() !!}'
                    },
                    success: function(data) {
                        //C1-$('#feeship').html(data[0]);
                        //C1-$('#grandtotal').html(data[1]);
                        $('.ship').html("Ship:")
                        $('#feeship').html(data).simpleMoneyFormat();
                        feeship = data;
                        $('.feeship_cart').val(data);
                        var grandtotal = parseInt(data) + parseInt(subtotal);
                        $('#grandtotal').html(grandtotal).simpleMoneyFormat();
                        $('.city_cart').val(city);
                        $('.province_cart').val(province);
                        $('.grandtotal_cart').val(grandtotal);

                    }
                })
            }
        }
    })
    $('#checkout').click(function() {
            var idUser = $(this).data("user");
            if (idUser == "") {
                swal({
                        title: "Bạn chưa đăng nhập!",
                        text: "Bạn có muốn đăng nhập không!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Đăng nhập!",
                        closeOnConfirm: false
                    },
                    function() {
                        window.location.href = "{{url('/show-login')}}";
                    });
            }
        })    
        $('.getCoupon').click(function() {
            if (product_id.length == 0) {
                swal("Mời bạn chọn sản phẩm!")
            } else {
                var coupon = $('.coupon').val();
                if (coupon == "") {
                    swal("Nhập mã giảm giá!")
                } else {
                    $.ajax({
                        url: '{{URL::to("/coupon")}}',
                        method: "POST",
                        data: {
                            coupon: coupon,
                            _token: '{!! csrf_token() !!}'
                        },
                        success: function(data) {
                            if (data.length == 3) {
                                swal("Kích hoạt mã thành công!",
                                    "Lưu ý: Một khi mã được kích hoạt thì không thể !")
                                var coupon_name = data[0];
                                var coupon_condition = data[1];
                                var coupon_number = data[2];
                                var number = "";
                                if (coupon_condition == 1) {
                                    var grandtotal = parseInt(subtotal) - parseInt(
                                            subtotal) * parseFloat(coupon_number) / 100 +
                                        parseInt(feeship);
                                    number = coupon_number + " %";
                                } else if (coupon_condition == 2) {
                                    var grandtotal = parseInt(subtotal) + parseInt(
                                        feeship) - parseInt(coupon_number);
                                    number = coupon_number + " K";

                                }
                                $('.grandtotal_cart').val(grandtotal);
                                $('#grandtotal').html(grandtotal).simpleMoneyFormat();
                                $('.coupon h5').html("Giảm giá:");
                                $('#coupon').html(number);
                                $('.coupon_cart').val(coupon_name);

                            } else {
                                swal("Mã giảm giá không tồn tại!");
                            }
                        }
                    })
                }
            }
        })
})
function updateQuanity($quanity, $id) {
        $.ajax({
            url: "{{URL::to('/update-quanity')}}",
            method: "POST",
            data: {
                id: $id,
                quanity: $quanity,
                product_id: product_id,
                _token: '{!! csrf_token() !!}'
            },
            success: function(data) {
                /* C1 if (data.length == 2) {
                    $('#total_' + $id).html(data[0]);
                    $('#subtotal').html(data[1]);
                    $('#grandtotal').html(data[1]);
                } else if (data.length == 3) {
                    $('#total_' + $id).html(data[0]);
                    $('#subtotal').html(data[1]);
                    $('#grandtotal').html(data[2]);
                } else {
                    $('#total_' + $id).html(data);

                }*/
                if (data.length == 2) {
                    $('#total_' + $id).html(data[0]).simpleMoneyFormat();
                    $('#subtotal').html(data[1]).simpleMoneyFormat();
                    subtotal = parseInt(data[1]);
                    var grandtotal = parseInt(data[1]) + parseInt(feeship);
                    $('#grandtotal').html(grandtotal).simpleMoneyFormat();
                } else {
                    $('#total_' + $id).html(data).simpleMoneyFormat();
                }
            }
        });


    }

</script>

@endpush