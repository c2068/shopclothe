<p>cartuser</p>
@if(!empty($cartUser))
@foreach($cartUser as $value)
<tr>
    <td>
        <img src="images/products/{{$value['image']}}" alt="">
    </td>
    <td>
        <div class="shop-details">
            <div class="productname">
                {{$value['name']}}
            </div>
            <p>
                <img alt="" src="images/star.png">
                <a class="review_num" href="#">
                    02 Review(s)
                </a>
            </p>
            <div class="color-choser">
                <span class="text">
                    Product Color :
                </span>
                <ul>
                    <li>
                        <a class="black-bg " href="#">
                            black
                        </a>
                    </li>
                    <li>
                        <a class="red-bg" href="#">
                            light red
                        </a>
                    </li>
                </ul>
            </div>
            <p>
                Product Code :
                <strong class="pcode">
                    Dress 120
                </strong>
            </p>
        </div>
    </td>
    <td>
        <h5>
            {{number_format($value['price'])}}
        </h5>
        VNĐ
    </td>
    <td>
        <input class="cart_quantity_input" style="width:75px" type="number" name="quantity"
            value="{{$value['quanity']}}" autocomplete="off" onchange="updateQuanity(this.value,'{{$value['idClothe']}}')">
    </td>
    <td>
        <h5>
            <strong class="red" id="total_{{$value['idClothe']}}">
                {{number_format($value['price'] * $value['quanity'])}}
            </strong>
        </h5>
    </td>
    <td>
        <a class="grab delete-product" style="cursor: -webkit-grab; cursor: grab;" data-id="{{$value['idClothe']}}">
            <img src="images/remove.png" alt="">
        </a>
    </td>
    <td><input type="checkbox" class="select_product" data-id="{{$value['idClothe']}}">
<input type="hidden" class="clothe_select_{{$value['idClothe']}}" name="clothe_select">
</td>
</tr>
@endforeach
@else
<tr colspan="5">
    <td style="font-size: 27px;color: #FE5252;font-weight: 500;">GIỎ HÀNG ĐANG TRỐNG</td>
</tr>
@endif