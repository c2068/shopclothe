@foreach($comments as $value)
<div class="row">
    <!--input type="hidden" name="" class="comment_{{$value->id}}" value="{{$value->id}}"-->
    <div class="col-md-1 col-sm-1">
        <img src="../FrontEnd/images/user/{{$value->customer_image}}" alt="">
    </div>
    <div class="col-md-8 col-sm-8">
        <div class="content">
            <span>{{$value->customer_name}}</span><span>1 giờ trước</span>
            <div id="myComment_{{$value->id}}" class="content_comment" data-id="{{$value->id}}">
                <div id="res_{{$value->id}}">
                    <p>{{$value->content}}</p>
                    <span>
                        <i id="liked_{{$value->id}}"
                            class="fa fa-thumbs-up {{Auth::check() && Auth::user()->Rate->Where('liked',1)->count() > 0 ? 'text-orange':''}}"
                            aria-hidden="true"></i>
                        <span id="like{{$value->id}}">{{$value->Rate->Where('liked',1)->count()}}</span>
                    </span>
                    <span>
                        <i id="disliked_{{$value->id}}"
                            class="fa fa-thumbs-down {{Auth::check() && Auth::user()->Rate->Where('liked',0)->count() > 0 ? 'text-orange':''}}"
                            aria-hidden="true"></i>
                        <span id="dislike{{$value->id}}">{{$value->Rate->Where('liked',0)->count()}}</span>
                    </span>
                    @if($value->customer_id != Session::get('idUser'))
                    <span class="onComment" data-id="{{$value->id}}">REPLY</span>
                    @endif
                </div>
                @if($value->customer_id == Session::get('idUser'))
                <div id="editcontent_{{$value->id}}" style="display: none">
                <textarea class="txt" name="" id="textReply_{{$value->id}}" cols="30" rows="2"  onkeyup="typeReply({{$value->id}})" ></textarea>
                <button class="closecomment" data-id="{{$value->id}}">HỦY</button>
                <button id="Reply_{{$value->id}}" class="savecomment" data-id="{{$value->id}}" disabled>LƯU</button>
                </div>
                @endif

            </div>
        </div>
    </div>
    <div id="reply_{{$value->id}}" class="reply">
        <div class="col-md-11 col-sm-11 col-md-offset-1">
            <div class="imgBox">
                @if(Auth::check())
                <img src="../FrontEnd/images/user/{{$value->customer_image}}" alt="">
                @else
                <img src="../FrontEnd/images/user/user.jpg" alt="">
                @endif
            </div>
            <div class="col-md-8 col-sm-8">
                <textarea name="" id="textReply_{{$value->id}}" cols="30" rows="2"
                    onkeyup="typeReply({{$value->id}})"></textarea>
            </div>
            @can('comment')
            <div class="col-md-3 col-sm-3">
                <button class="offComment deleteComment" data-id="{{$value->id}}">HỦY</button>
                <button id="Reply_{{$value->id}}" class="addReply" data-id="{{$value->id}}" disabled>BÌNH LUẬN</button>
            </div>
            @endcan
            @cannot('comment')
            <div class="col-md-3 col-sm-3">
                <button class="deleteComment"><a href="{{route('login')}}"> Đăng nhập để bình luận</a></button>

            </div>
            @endcan
        </div>
    </div>

    @if($value->customer_id == Session::get('idUser'))
    <div class="edit" data-bs-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></div>
    <ul class="dropdown-menu" style="top:-10px; left: 60%">
        <li><a href="" class="dropdown-item" id="editComment" data-id="{{$value->id}}">Chỉnh sửa</a></li>
        <li><a href="" class="dropdown-item" id="deleteComment" data-id="{{$value->id}}"
                data-idclothe="{{$value->clothe_id}}">Xóa</a></li>
    </ul>
    @endif
    @if($value->ListReply->count() >0)
    <div class="scrolldown col-md-11 col-sm-11 col-md-offset-1" data-id="{{$value->id}}">
        <i class="fa fa-caret-down list_{{$value->id}}" aria-hidden="true"></i>
        <span class="list_{{$value->id}}">Xem thêm {{$value->ListReply->count()}} bình luận</span>
    </div>
    <div class="list-reply-{{$value->id}} list-reply">
    </div>
    @endif

</div>
@endforeach
@push('styles')
<style type="text/css">
.text-orange {
    color: #f7544a;
}

.text-black {
    color: #000000;
}
.txt{
    margin-bottom: 5px;
    border: none;
    border-bottom: solid 1px #cccccc;
    border-radius: 0;
    padding: 0;
    height: 31px;
    padding-top: 10px;
}

</style>
@endpush