<div class="row">
    <div class="col-md-1 col-sm-1">
        @if(Auth::check())
        <img src="../FrontEnd/images/user/{{Session::get('imageUser')}}" alt="">
        @else
        <img src="../FrontEnd/images/user/user.jpg" alt="">
        @endif
    </div>
    <div class="col-md-8 col-sm-8">
        <textarea name="" id="textComment" cols="30" rows="2" onkeyup="typeComment()"></textarea>
    </div>
    @can('comment')
    <div class="col-md-3 col-sm-3">
        <button class="deleteComment">HỦY</button>
        <button id="Comment" data-id="{{$clothe->id}}" disabled>BÌNH LUẬN</button>
    </div>
    @endcan
    @cannot('comment')
    <div class="col-md-3 col-sm-3">
        <button class="deleteComment"><a href="{{route('login')}}"> Đăng nhập để bình luận</a></button>

    </div>
    @endcan
</div>
<div id="list-comment">
    @include('pages.comment.list_comment')
</div>
@push('scripts')
<script type="text/javascript">
var $ = jQuery.noConflict();
$(document).ready(function() {
    $(document).on('click', '.addReply', function() {
        var id = $(this).data("id");
        var text = document.getElementById("textReply_" + id).value;
        $.ajax({
            url: "{{url('/add-reply')}}",
            method: "post",
            data: {
                id: id,
                content: text,
                _token: '{!! csrf_token() !!}'
            },
            success: function(data) {
                document.getElementById("reply_" + id).style.display = "none";
                console.log(data);
            }
        })
    })
    $(document).on('click', '.onComment', function() {
        var id = $(this).data("id");
        document.getElementById("reply_" + id).style.display = "block";
    })
    $(document).on('click', '.offComment', function() {
        var id = $(this).data("id");
        document.getElementById("reply_" + id).style.display = "none";
    })
    
    $(document).on('click', '#deleteComment', function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        var idclothe = $(this).data("idclothe");
        $.ajax({
            url: "{{url('/deleteComment')}}",
            method: "post",
            data: {
                id: id,
                idclothe: idclothe,
                _token: "{!! csrf_token() !!}"
            },
            success: function(data) {
                $('#list-comment').html(data);
            }
        })
    })
    $(document).on('click', '#deleteReply', function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        var idcomment = $(this).data("idcomment");
        $.ajax({
            url: "{{url('/deleteReply')}}",
            method: "post",
            data: {
                id: id,
                idcomment: idcomment,
                _token: "{!! csrf_token() !!}"
            },
            success: function(data) {
                $(".list-reply-" + idcomment).html(data);
            }
        })
    })
    $(document).on('click', '#editReply', function(e) {
        e.preventDefault();
        var idreply = $(this).parents("ul").data("id");
        document.getElementById("resReply_"+idreply).style.display = "none";
        document.getElementById("editcontentReply_"+idreply).style.display = "block";

    })
    $(document).on('click', '.closeReply', function() {
        var id = $(this).data("id");
        document.getElementById("resReply_" + id).style.display = "block";
        document.getElementById("editcontentReply_" + id).style.display = "none";

    })
    $(document).on('click', '#editComment', function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        /*var html =
            '<textarea name="" id="textReply_' + id +
            '"cols="30" rows="2"  onkeyup="typeReply(id)" ></textarea><button class="offComment deleteComment" data-id="id">HỦY</button><button id="Reply_id" class="addReply" data-id="id" disabled>BÌNH LUẬN</button>';
        $("#myComment_" + id).html(html);*/
        var m =  $('#res_'+id+" p").html();
        document.getElementById("res_" + id).style.display = "none";
        $('.txt').html(m);
        document.getElementById("editcontent_" + id).style.display = "block";

    })
    $(document).on('click', '.closecomment', function() {
        var id = $(this).data("id");
        document.getElementById("res_" + id).style.display = "block";
        document.getElementById("editcontent_" + id).style.display = "none";

    })
  
    $('.savecomment').click(function(){
        var idcomment = $(this).parents(".content_comment").data("id");
        var txt = $('.txt').val();
        var url = "{{ route('savecomment', ":id") }}";
        url = url.replace(':id', idcomment);
        $.ajax({
            url: url,
            method: "get",
            data: {
                text: txt,
            },
            success: function(data){
             document.getElementById("editcontent_" + idcomment).style.display = "none";
             document.getElementById("res_" + idcomment).style.display = "block";
             $('#res_'+idcomment+" p").html(txt);
            }
        })
    })
    $(document).on('click', '.scrolldown', function() {
        var idcomment = $(this).data("id");
        $.ajax({
            url: "{{url('/list-reply')}}",
            method: "post",
            data: {
                idcomment: idcomment,
                _token: '{!! csrf_token() !!}'
            },

            success: function(data) {
                $(".list-reply-" + idcomment).html(data);
                $('.scrolldown span.list_' + idcomment).html("Ẩn bình luận");
                $('.scrolldown i.list_' + idcomment).removeClass("fa-caret-down");
                $('.scrolldown i.list_' + idcomment).addClass("fa-caret-up");
                $(".scrolldown").addClass("scrollup");
                $(".scrollup").removeClass("scrolldown");
            }
        })
    })
    $(document).on('click', '.scrollup', function() {
        var idcomment = $(this).data("id");
        $('.scrollup span.list_' + idcomment).html("Xem thêm bình luận");
        $('.scrollup i.list_' + idcomment).removeClass("fa-caret-up");
        $('.scrollup i.list_' + idcomment).addClass("fa-caret-down");
        $(".scrollup").addClass("scrolldown");
        $(".scrolldown").removeClass("scrollup");
        $(".list-reply-" + idcomment).html("");

    })
    /*$('.fa-thumbs-up').click(function(){
        var idcomment = $(this).data("id");
        $.ajax({
            url: "{{url('/change-rate')}}",
            method: "post",
            data: {
                idcomment: idcomment,
                _token: '{!! csrf_token() !!}'
            },
            success: function(data) {
                $(".list-reply-" + idcomment).html(data);
                $('.scrolldown span.list_' + idcomment).html("Ẩn bình luận");
                $('.scrolldown i.list_' + idcomment).removeClass("fa-caret-down");
                $('.scrolldown i.list_' + idcomment).addClass("fa-caret-up");
                $(".scrolldown").addClass("scrollup");
                $(".scrollup").removeClass("scrolldown");
            }
        })
    })*/
    $('i.fa-thumbs-up').click(function() {
        var idcomment = $(this).parents(".content_comment").data("id");
        var l = $('#like' + idcomment + '').html();
        var dl = $('#dislike' + idcomment + '').html();
        var obj = $(this);
        var url = "{{ route('liked', ":id") }}";
        url = url.replace(':id', idcomment);
        $.ajax({
            url: url,
            method: "get",
            data: {},
            success: function(data) {
                if (data.status == 'success') {
                    if (data.action == 'add_like') {
                        $('#like' + idcomment + '').html(parseInt(l) + 1);
                        $(obj).addClass('text-orange');
                    } else if (data.action == 'destroy_like') {
                        $('#like' + idcomment + '').html(parseInt(l) - 1);
                        $(obj).removeClass('text-orange');
                    } else if (data.action == 'switch_like') {
                        $('#like' + idcomment + '').html(parseInt(l) + 1);
                        $('#dislike' + idcomment + '').html(parseInt(dl) - 1);
                        $(obj).addClass('text-orange');
                        $('#disliked_' + idcomment).removeClass('text-orange');

                    }
                }
            }
        })
    })
    $('i.fa-thumbs-down').click(function() {
        var idcomment = $(this).parents(".content_comment").data("id");
        var l = $('#dislike' + idcomment + '').html();
        var dl = $('#like' + idcomment + '').html();
        var obj = $(this);

        var url = "{{ route('disliked', ":id ") }}";
        url = url.replace(':id', idcomment);
        $.ajax({
            url: url,
            method: "get",
            data: {},
            success: function(data) {
                if (data.status == 'success') {
                    if (data.action == 'add_dislike') {
                        $('#dislike' + idcomment + '').html(parseInt(l) + 1);
                        $(obj).addClass('text-orange');
                    } else if (data.action == 'destroy_dislike') {
                        $('#dislike' + idcomment + '').html(parseInt(l) - 1);
                        $(obj).removeClass('text-orange');
                    } else if (data.action == 'switch_dislike') {
                        $('#dislike' + idcomment + '').html(parseInt(l) + 1);
                        $('#like' + idcomment + '').html(parseInt(dl) - 1);
                        $(obj).addClass('text-orange');
                        $('#liked_' + idcomment).removeClass('text-orange');
                        $
                    }
                }
            }
        })
    })
})

function typeReply($id) {
    var text = document.getElementById("textReply_" + $id).value;
    if (text == '') {
        $('#Reply_' + $id).removeClass("comment");
        document.getElementById('Reply_' + $id).disabled = true;

    } else {
        $('#Reply_' + $id).addClass("comment");
        document.getElementById('Reply_' + $id).disabled = false;
    }
}
</script>
@endpush