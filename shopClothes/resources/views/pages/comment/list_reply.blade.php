@foreach($listcomment as $value)
<div class="col-md-11 col-sm-11 col-md-offset-1">
    <div class="">
        <img src="../FrontEnd/images/user/avartar.jpg" alt="">
    </div>
    <div class="col-md-8 col-sm-8">
        <div class="content">
            <span>{{$value->customer_name}}</span><span>1 giờ trước</span>
            <p id="resReply_{{$value->id}}">
                <span>{{$value->content}}</span>|
                <span><i class="fa fa-thumbs-up" aria-hidden="true"></i>{{$value->like}}</span>
                <span><i class="fa fa-thumbs-down" aria-hidden="true"></i>{{$value->dislike}}</span>
                <span class="onComment">REPLY</span>
            </p>
            @if($value->customer_id == Session::get('idUser'))
                <div id="editcontentReply_{{$value->id}}" style="display:none">
                <textarea class="txt" name="" id="textReply_{{$value->id}}" cols="30" rows="2"  onkeyup="typeReply({{$value->id}})" ></textarea>
                <button class="closeReply" data-id="{{$value->id}}">HỦY</button>
                <button id="Reply_{{$value->id}}" class="savecomment" data-id="{{$value->id}}" disabled>LƯU</button>
                </div>
                @endif
        </div>
    </div>
    @if($value->customer_id == Session::get("idUser"))
    <div class="edit" data-bs-toggle="dropdown"><i class="fa fa-ellipsis-v" aria-hidden="true"></i></div>
    <ul class="dropdown-menu" style="top:-10px; left: 60%" data-id="{{$value->id}}">
       <li><a href="" class="dropdown-item" id="editReply" >Chỉnh sửa</a></li>
       <li><a href="" class="dropdown-item" id="deleteReply" data-id="{{$value->id}}" data-idcomment="{{$value->comment_id}}">Xóa</a></li>
    </ul>
    @endif
</div>
@endforeach