<ul class="products-listItem">
    @foreach($product_category as $value)
    <?php $last_id = $value->id?>
    <li class="products">
        <div class="offer">
            New
        </div>
        <div class="thumbnail">
            <a href="{{URL::to('detail/'.$value->id)}}">
                <img src="images/products/{{$value->image}}" alt="Product Name">
            </a>
        </div>
        <div class="product-list-description">
            <div class="productname">
                {{$value->name}}
            </div>
            <p>
                <img src="images/star.png" alt="">
                <a href="#" class="review_num">
                    02 Review(s)
                </a>
            </p>
            <p>
                Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante
                ipsum primis in faucibus orci luctus et ultri ces posuere cubilia curae. Proin lectus ipsum, gravida
                etds mattis vulputate, tristique ut lectus. Sed et lorem nunc...
            </p>
            <div class="list_bottom">
                <div class="price">
                    <span class="new_price">
                        {{$value->price}}
                        <sup>
                            $
                        </sup>
                    </span>
                    <span class="old_price">
                        450.00
                        <sup>
                            $
                        </sup>
                    </span>
                </div>
                <input type="hidden" class="id_{{$value->id}}" value="{{$value->id}}">
                <input type="hidden" class="name_{{$value->id}}" value="{{$value->name}}">
                <input type="hidden" class="image_{{$value->id}}" value="{{$value->image}}">
                <input type="hidden" class="price_{{$value->id}}" value="{{$value->price}}">
                <div class="button_group">
                <button class="button add-cart"  data-id="{{$value->id}}" type="button">Add To Cart</button>
                    <button class="button compare">
                        <i class="fa fa-exchange">
                        </i>
                    </button>
                    <button class="button favorite">
                        <i class="fa fa-heart-o">
                        </i>
                    </button>
                </div>
            </div>
        </div>
    </li>
    @endforeach
</ul>
<button id="load_more" data-id="{{$last_id}}">Load more</button>