<?php 
   if($product_category[0]["idCate"]==1){
       $url = "images/products/womenNew";
   }
   else{
    $url = "images/products/menNew";

   }
?>
<div class="row" id="list">
    @foreach($product_category as $value)
    <div class="col-md-4 col-sm-6">
        <div class="products">
            <div class="thumbnail">
                <a href="{{URL::to('detail/'.$value->id)}}">
                    <img src="<?php echo $url;?>/{{$value->image}}" alt="Product Name">
                </a>
            </div>
            <div class="productname">
                {{$value->name}}
            </div>
            <h4 class="price">
                {{number_format($value->price)}}
            </h4>
            <input type="hidden" class="id_{{$value->id}}" value="{{$value->id}}">
            <input type="hidden" class="name_{{$value->id}}" value="{{$value->name}}">
            <input type="hidden" class="image_{{$value->id}}" value="{{$value->image}}">
            <input type="hidden" class="price_{{$value->id}}" value="{{$value->price}}">
            <div class="button_group">
                <button class="button add-cart"  data-id="{{$value->id}}" type="button">Add To Cart</button>
                <button class="button compare" type="button">
                    <i class="fa fa-exchange">
                    </i>
                </button>
                <button class="button wishlist" type="button">
                    <i class="fa fa-heart-o">
                    </i>
                </button>
            </div>
        </div>
    </div>
    @endforeach
</div>
<div class="col-md-4 col-sm-6 cong" style="margin-bottom: 10px"> {{ $product_category->links() }}</div>
<style>
.w-5 {
    display: none;
}

nav div.sm:flex {
    display: none;
}
</style>