@extends('welcome')
@section('content')
<div class="container_fullwidth">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="category leftbar">
                    <h3 class="title">
                        Categories
                    </h3>
                    <ul>
                        @foreach($category as $value)
                        <li>
                            @if($value->id == $product_category[0]["idCate"])
                            <a href="#" style="font-weight: 500; color: #FE5252">
                                {{$value->title}}
                            </a>
                            @else
                            <a href="#">
                                {{$value->title}}
                            </a>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="clearfix">
                </div>
                <div class="branch leftbar">
                    <h3 class="title">
                        Branch
                    </h3>
                    <ul>
                        <li>
                            <a href="#">
                                New
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Sofa
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Salon
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                New Trend
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Living room
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Bed room
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix">
                </div>
                <div class="price-filter leftbar">
                    <h3 class="title">
                        Price
                    </h3>
                    <form class="pricing">
                        <label>
                            $
                            <input type="number">
                        </label>
                        <span class="separate">
                            -
                        </span>
                        <label>
                            $
                            <input type="number">
                        </label>
                        <input type="submit" value="Go">
                    </form>
                </div>
                <div class="clearfix">
                </div>
                <div class="clolr-filter leftbar">
                    <h3 class="title">
                        Color
                    </h3>
                    <ul>
                        <li>
                            <a href="#" class="red-bg">
                                light red
                            </a>
                        </li>
                        <li>
                            <a href="#" class=" yellow-bg">
                                yellow"
                            </a>
                        </li>
                        <li>
                            <a href="#" class="black-bg ">
                                black
                            </a>
                        </li>
                        <li>
                            <a href="#" class="pink-bg">
                                pink
                            </a>
                        </li>
                        <li>
                            <a href="#" class="dkpink-bg">
                                dkpink
                            </a>
                        </li>
                        <li>
                            <a href="#" class="chocolate-bg">
                                chocolate
                            </a>
                        </li>
                        <li>
                            <a href="#" class="orange-bg">
                                orange-bg
                            </a>
                        </li>
                        <li>
                            <a href="#" class="off-white-bg">
                                off-white
                            </a>
                        </li>
                        <li>
                            <a href="#" class="extra-lightgreen-bg">
                                extra-lightgreen
                            </a>
                        </li>
                        <li>
                            <a href="#" class="lightgreen-bg">
                                lightgreen
                            </a>
                        </li>
                        <li>
                            <a href="#" class="biscuit-bg">
                                biscuit
                            </a>
                        </li>
                        <li>
                            <a href="#" class="chocolatelight-bg">
                                chocolatelight
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix">
                </div>
                <div class="product-tag leftbar">
                    <h3 class="title">
                        Products
                        <strong>
                            Tags
                        </strong>
                    </h3>
                    <ul>
                        <li>
                            <a href="#">
                                Lincoln us
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                SDress for Girl
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Corner
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Window
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                PG
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Oscar
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Bath room
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                PSD
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix">
                </div>
                <div class="others leftbar">
                    <h3 class="title">
                        Others
                    </h3>
                </div>
                <div class="clearfix">
                </div>
                <div class="others leftbar">
                    <h3 class="title">
                        Others
                    </h3>
                </div>
                <div class="clearfix">
                </div>
                <div class="fbl-box leftbar">
                    <h3 class="title">
                        Facebook
                    </h3>
                    <span class="likebutton">
                        <a href="#">
                            <img src="images/fblike.png" alt="">
                        </a>
                    </span>
                    <p>
                        12k people like Flat Shop.
                    </p>
                    <ul>
                        <li>
                            <a href="#">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                            </a>
                        </li>
                    </ul>
                    <div class="fbplug">
                        <a href="#">
                            <span>
                                <img src="images/fbicon.png" alt="">
                            </span>
                            Facebook social plugin
                        </a>
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <div class="leftbanner">
                    <img src="images/banner-small-01.png" alt="">
                </div>
            </div>
            <div class="col-md-9">
                <div class="banner">
                    <div class="bannerslide" id="bannerslide">
                        <ul class="slides">
                            <li>
                                <a href="#">
                                    <img src="images/banner-01.jpg" alt="" />
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="images/banner-02.jpg" alt="" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix">
                </div>
                <div class="products-grid">
                    <div class="toolbar">
                        <div class="sorter">
                            <div class="view-mode">
                                <a class="list">
                                    List
                                </a>
                                <a class="grid active">
                                    Grid
                                </a>
                            </div>
                            <div class="sort-by">
                                Sort by :
                                <select class="sort">
                                    <option value="Default" selected>
                                        Default
                                    </option>
                                    <option value="ASC" >
                                        Giá tăng dần
                                    </option>
                                    <option value="DESC">
                                       Giá giảm dần
                                    </option>
                                </select>
                            </div>
                            <div class="limiter">
                                Show :
                                <select name="">
                                    <option value="3" selected>
                                        3
                                    </option>
                                    <option value="6">
                                        6
                                    </option>
                                    <option value="9">
                                        9
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="pager">
                            <a href="#" class="prev-page">
                                <i class="fa fa-angle-left">
                                </i>
                            </a>
                            <a href="#" class="active">
                                1
                            </a>
                            <a href="#">
                                2
                            </a>
                            <a href="#">
                                3
                            </a>
                            <a href="#" class="next-page">
                                <i class="fa fa-angle-right">
                                </i>
                            </a>
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                    <div class="" id="linh">
                    @include('pages.category.category_product_grid')
                    </div>
                    <input type="hidden" class="category-current" value="{{$product_category[0]['idCate']}}">                   
                     <div class="clearfix">
                    </div>

                    <div class="toolbar">
                        <div class="sorter bottom">
                        <div class="view-mode">
                                <a class="list">
                                    List
                                </a>
                                <a class="grid active">
                                    Grid
                                </a>
                            </div>
                            <div class="sort-by">
                                Sort by :
                                <select class="sort">
                                    <option value="Default" selected>
                                        Default
                                    </option>
                                    <option value="ASC" >
                                        Giá tăng dần
                                    </option>
                                    <option value="DESC">
                                       Giá giảm dần
                                    </option>
                                </select>
                            </div>
                            <div class="limiter">
                                Show :
                                <select name="">
                                    <option value="3" selected>
                                        3
                                    </option>
                                    <option value="6">
                                        6
                                    </option>
                                    <option value="9">
                                        9
                                    </option>
                                </select>
                            </div>
                        </div>
                       
                 
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
        </div>
        <div class="our-brand">
            <h3 class="title">
                <strong>
                    Our
                </strong>
                Brands
            </h3>
            <div class="control">
                <a id="prev_brand" class="prev" href="#">
                    &lt;
                </a>
                <a id="next_brand" class="next" href="#">
                    &gt;
                </a>
            </div>
            <ul id="braldLogo">
                <li>
                    <ul class="brand_item">
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/envato.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/themeforest.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/photodune.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/activeden.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/envato.png" alt="">
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <ul class="brand_item">
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/envato.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/themeforest.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/photodune.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/activeden.png" alt="">
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="brand-logo">
                                    <img src="images/envato.png" alt="">
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    var $ = jQuery.noConflict();
    $(document).ready(function() {
        $('.list').click(function() {
            var category = $('.category-current').val();
            $(this).addClass("active");
            var url = "/shopClothes/layout_product_list"
            $(".grid").removeClass("active");
            $.ajax({
                url: url,
                data: {
                    category: category
                },
                success: function(data) {
                    $('#linh').html(data);
                    //console.log(data);
                }
            });
        })
        $('.grid').click(function() {
            var category = $('.category-current').val();
            $(this).addClass("active");
            $(".list").removeClass("active");
            var url = "/shopClothes/category/" + category;
            $.ajax({
                url: url,
                method: "GET",
                success: function(data) {
                    $('#linh').html(data);
                    //console.log(data);
                }
            });
        })
        $('.sort').change(function() {
            var option = $(this).val();
            var category = $('.category-current').val();
            fetch_data_sort(1, category, option);
        })
        $(document).on('click', '#load_more', function() {
            var last_record = $(this).data("id");
            var category = $('.category-current').val();
            $(this).html('<b>Loading.....</b>');
            load_data(last_record, category);


        })
        $(document).on('click', '.cong a', function(event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            var option = $('.sort').val();
            var category = $('.category-current').val();
            if (option == 'Default') {
                fetch_data(page, category);
            } else {
                fetch_data_sort(page, category, option);
            }

        })
    })
    function load_data(lastRecord, category) {
        $.ajax({
            url: "{{url('/load_data')}}",
            method: "POST",
            data: {
                last_record: lastRecord,
                category: category,
                _token: '{!! csrf_token() !!}'
            },
            success: function(data) {
                $('#load_more').remove();
                $('#linh').append(data);
            }
        })
    }

    function fetch_data(page, category) {
        history.pushState({}, null, 'http://localhost:8080/shopClothes/category/' + category + '?page=' + page);
        var url = "/shopClothes/category/" + category + "?page=" + page;
        $.ajax({
            url: url,
            success: function(data) {
                $('#linh').html(data);
                //console.log(data);  
            }
        })

    }

    function fetch_data_sort(page, category, sorttype) {
        history.pushState({}, null, 'http://localhost:8080/shopClothes/category/' + category + '?page=' + page +
            "&sortby=price&sorttype=" + sorttype);
        $.ajax({
            url: "/shopClothes/fetch_data_sort?page=" + page,
            data: {
                category: category,
                sorttype: sorttype
            },
            success: function(data) {
                $('#linh').html(data);
                //console.log(data);
            }
        })
    }
</script>
@endpush